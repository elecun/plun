################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../include/core/component_broker.cpp \
../include/core/component_driver.cpp \
../include/core/component_manager.cpp \
../include/core/errorcode.cpp \
../include/core/message.cpp \
../include/core/xmlprofile.cpp 

OBJS += \
./include/core/component_broker.o \
./include/core/component_driver.o \
./include/core/component_manager.o \
./include/core/errorcode.o \
./include/core/message.o \
./include/core/xmlprofile.o 

CPP_DEPS += \
./include/core/component_broker.d \
./include/core/component_driver.d \
./include/core/component_manager.d \
./include/core/errorcode.d \
./include/core/message.d \
./include/core/xmlprofile.d 


# Each subdirectory must supply rules for building sources it contributes
include/core/%.o: ../include/core/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -D__cplusplus=201103L -I/usr/include -I/usr/local/include -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


