
* Tools list

Belows are not support yet, but now planned.

(1) pcons : plun console to access the plun engine (not support yet / planned)
     프레임워크 엔진을 종료하지 않은 상태에서 접속하여 콘솔을 통해 직접 제어하거나, 값을 읽어볼 수 있도록 하기위한 도구
(2) pcons_vis : plun console with visualization to access the plun engine (not support yet / not plaaned)
     pcons의 visual 버젼
(3) pcc : generate plun components by using template (not support yet / planned)
     새로운 컴포넌트를 생성하기위한 툴(template 기반)
(4) plm : plun license manager (not support yet / planned)
     컴포넌트별로 가지고 있는 라이센스들을 관리하는 서비스 (필수 서비스)
(5) pdm : plun download manager (not support yet / planned)
     시스템에 맞는 컴포넌트들을 찾고, 다운로드하여 설치하는 서비스 (필수 서비스)
(6) pcr : plun component repository (not support yet / planned)
     서비스 망내 설치가된 컴포넌트들을 보관하고 버젼을 관리하는 서비스