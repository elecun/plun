################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../include/extra/spdlog/details/format.cc 

CC_DEPS += \
./include/extra/spdlog/details/format.d 

OBJS += \
./include/extra/spdlog/details/format.o 


# Each subdirectory must supply rules for building sources it contributes
include/extra/spdlog/details/%.o: ../include/extra/spdlog/details/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -D__cplusplus=201103L -I/usr/include -I/usr/local/include -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


