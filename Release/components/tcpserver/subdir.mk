################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../components/tcpserver/libtcpserver.cpp \
../components/tcpserver/session.cpp \
../components/tcpserver/tcpserver.cpp 

OBJS += \
./components/tcpserver/libtcpserver.o \
./components/tcpserver/session.o \
./components/tcpserver/tcpserver.o 

CPP_DEPS += \
./components/tcpserver/libtcpserver.d \
./components/tcpserver/session.d \
./components/tcpserver/tcpserver.d 


# Each subdirectory must supply rules for building sources it contributes
components/tcpserver/%.o: ../components/tcpserver/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -I/usr/include -I/usr/local/include -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


