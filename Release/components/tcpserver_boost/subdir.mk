################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../components/tcpserver_boost/libtcpserver.cpp \
../components/tcpserver_boost/session.cpp \
../components/tcpserver_boost/tcpserverboost.cpp 

OBJS += \
./components/tcpserver_boost/libtcpserver.o \
./components/tcpserver_boost/session.o \
./components/tcpserver_boost/tcpserverboost.o 

CPP_DEPS += \
./components/tcpserver_boost/libtcpserver.d \
./components/tcpserver_boost/session.d \
./components/tcpserver_boost/tcpserverboost.d 


# Each subdirectory must supply rules for building sources it contributes
components/tcpserver_boost/%.o: ../components/tcpserver_boost/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -I/usr/include -I/usr/local/include -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


