################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../components/tca6416a/libtca6416a.cpp \
../components/tca6416a/tca6416a.cpp 

OBJS += \
./components/tca6416a/libtca6416a.o \
./components/tca6416a/tca6416a.o 

CPP_DEPS += \
./components/tca6416a/libtca6416a.d \
./components/tca6416a/tca6416a.d 


# Each subdirectory must supply rules for building sources it contributes
components/tca6416a/%.o: ../components/tca6416a/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -I/usr/include -I/usr/local/include -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


