################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../components/log/spdlog/details/format.cc 

CC_DEPS += \
./components/log/spdlog/details/format.d 

OBJS += \
./components/log/spdlog/details/format.o 


# Each subdirectory must supply rules for building sources it contributes
components/log/spdlog/details/%.o: ../components/log/spdlog/details/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -I/usr/include -I/usr/local/include -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


