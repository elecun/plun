/*
 * plunframework.hpp
 *
 *  Created on: 2015. 1. 27
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 */

#ifndef INCLUDE_PLUNFRAMEWORK_HPP_
#define INCLUDE_PLUNFRAMEWORK_HPP_


#ifndef BOOST_ALL_DYN_LINK
#define BOOST_ALL_DYN_LINK
#endif


#if defined(__unix__) || defined(__gnu_linux__) || defined(linux) || defined(__linux__)

#include "core/component_manager.hpp"
#include "core/component_broker.hpp"
#include "core/version.hpp"
#include "core/errorcode.hpp"

#include "../include/extra/spdlog/spdlog.h"
#include "../include/extra/format.h"

using namespace plunframework;
using namespace boost;
using namespace spdlog;


#endif


#endif /* INCLUDE_PLUNFRAMEWORK_HPP_ */
