/*
 * component_broker.cpp
 *
 *  Created on: 2015. 3. 14.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 */

#include "component_broker.hpp"

namespace plunframework {
namespace broker {

component_broker* component_broker::_instance = nullptr;

component_broker* component_broker::get()
{
	if(!_instance)
		_instance = new component_broker;
	return _instance;
}

void component_broker::destroy()
{
	_topic_map.clear();

	if(_instance)
		delete _instance;
}

} /* namespace broker */
} /* namespace plunframework */
