/*
 * component_driver.hpp
 *
 *  Created on: 2015. 3. 2.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Component Driver
 */

#ifndef INCLUDE_CORE_COMPONENT_DRIVER_HPP_
#define INCLUDE_CORE_COMPONENT_DRIVER_HPP_

#include "./interface/icomponent_base.hpp"
#include "message.hpp"
#include <queue>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <tuple>

using namespace std;

namespace plunframework {
namespace driver {

class component_driver {
public:
	component_driver(const char* component_name);
	virtual ~component_driver();

	void run();
	void stop();
	bool loop();

	interface::icomponent_base* get_component() const { return _ptr_component; }

	bool valid() { return _handle!=nullptr; }

	//[Tech note] implementation of non-specialized template must be visible.
	template<typename... Args>
	void request(const char* head, const Args&... args) {
		auto data = std::make_tuple(head, args...);
		message::message msg;
		message::pack(&msg, data);

		_request_box.push(msg);
		_condition.notify_one();
	}

private:
	bool load(const char* component_name);
	void unload();
	void request_process();

private:
	interface::icomponent_base* _ptr_component = nullptr;
	void* _handle = nullptr;
	boost::mutex _mutex;

	std::queue<message::message> _request_box;
	boost::condition_variable _condition;

	process::task _request_task;
	process::task _loop_task;

};


} /* namespace driver */
} /* namespace plunframework */

#endif /* INCLUDE_CORE_COMPONENT_DRIVER_HPP_ */
