/*
 * icomponent_gui.hpp
 *
 *  Created on: 2015. 5. 16.
 *      Author: hwang-linux
 */

#ifndef INCLUDE_CORE_INTERFACE_ICOMPONENT_GUI_HPP_
#define INCLUDE_CORE_INTERFACE_ICOMPONENT_GUI_HPP_

#include "icomponent_runnable.hpp"
#include "../component_broker.hpp"

namespace plunframework {
namespace interface {

class icomponent_gui : public icomponent_runnable
{
public:
	icomponent_gui(const char* component_name, int argc=0, char** argv=nullptr)
	:icomponent_runnable(component_name, types::comp_type::GUI) { }
		virtual ~icomponent_gui() { }

};

}
}



#endif /* INCLUDE_CORE_INTERFACE_ICOMPONENT_GUI_HPP_ */
