/*
 * icomponent_assist.hpp
 *
 *  Created on: 2015. 3. 1.
 *      Author: Byunghun Hwang<bhhwnag@nsynapse.com>
 *
 *      Assist Component as a Library
 */

#ifndef INCLUDE_CORE_ICOMPONENT_ASSIST_HPP_
#define INCLUDE_CORE_ICOMPONENT_ASSIST_HPP_

#include "icomponent_base.hpp"

namespace plunframework {
namespace interface {

class icomponent_assist : public icomponent_base
{
public:
	icomponent_assist(const char* component_name)
	:icomponent_base(component_name, types::comp_type::ASSIST) { }

	virtual ~icomponent_assist() { }

};

}
}



#endif /* INCLUDE_CORE_ICOMPONENT_ASSIST_HPP_ */
