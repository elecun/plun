/*
 * icomponent_runnable.hpp
 *
 *  Created on: 2015. 3. 1.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Runnable Component Interface
 */

#ifndef INCLUDE_CORE_ICOMPONENT_RUNNABLE_HPP_
#define INCLUDE_CORE_ICOMPONENT_RUNNABLE_HPP_

#include "icomponent_base.hpp"

namespace plunframework {
namespace interface {

class icomponent_runnable : public icomponent_base {
public:
	icomponent_runnable(const char* component_name, types::comp_type type)
	:interface::icomponent_base(component_name, type)
	{

	}
	virtual ~icomponent_runnable() { }

	/*
	 * loop interface
	 * @return	false breaking loop
	 */
	virtual bool loop() = 0;

};

}
}



#endif /* INCLUDE_CORE_ICOMPONENT_RUNNABLE_HPP_ */
