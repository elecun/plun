/*
 * icomponent_app.hpp
 *
 *  Created on: 2015. 3. 1.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Application Component Interface
 */

#ifndef INCLUDE_CORE_ICOMPONENT_APP_HPP_
#define INCLUDE_CORE_ICOMPONENT_APP_HPP_

#include "icomponent_runnable.hpp"

namespace plunframework {
namespace interface {

class icomponent_app : public icomponent_runnable
{
public:
	icomponent_app(const char* component_name)
	:icomponent_runnable(component_name, types::comp_type::APP) { }
		virtual ~icomponent_app() { }

};

}
}

#endif /* INCLUDE_CORE_ICOMPONENT_APP_HPP_ */
