/*
 * icomponent_base.hpp
 *
 *  Created on: 2015. 3. 1.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Base Component Interface
 */

#ifndef INCLUDE_CORE_ICOMPONENT_BASE_HPP_
#define INCLUDE_CORE_ICOMPONENT_BASE_HPP_

#include "iprofile.hpp"
#include "../errorcode.hpp"
#include "../typedef.hpp"
#include "../macro.hpp"
#include "../message.hpp"
#include <unordered_map>
#include <memory>
#include <string>

#include "../../extra/spdlog/spdlog.h"

using namespace std;
using namespace spdlog;

namespace plunframework {
namespace interface {

typedef std::unordered_map<profile::section*, pair<string, string>> profile_map;

class icomponent_base
{
public:
	virtual ~icomponent_base() {
		if(_profile)
			delete _profile;

		if(_log)
			spdlog::drop(_name);
	}

	/*
	 * setup interface (initialization)
	 * when it is installed, system calls setup() first.
	 * @return		true if it is successfully installed.
	 */
	virtual bool setup(error::errorcode& e, int argc=0, char** argv=nullptr) = 0;

	/*
	 * run/stop interface
	 * run and stop service
	 */
	virtual bool run(error::errorcode& e) = 0;
	virtual bool stop(error::errorcode& e) = 0;

	/*
	 * Requested message processing
	 */
	virtual void request(message::message* msg) = 0;

	/*
	 * supporting default functions
	 */
	const char* get_name() { return _name.c_str(); }
	types::comp_type get_type() { return _type; }
	types::comp_status get_status() { return _status; }
	iprofile* get_profile() { return _profile; }
	bool set_profile(iprofile* profile, const char* path) {
		_profile = profile;
		return _profile->load(path);
	}
	void set_status(types::comp_status status) { _status = status; }

protected:
	explicit icomponent_base(const char* component_name, types::comp_type type)
	:_type(type), _status(types::comp_status::READY),
	 _name(component_name), _profile(nullptr), _log(spdlog::stdout_logger_mt(component_name))
	{

	}

	std::shared_ptr<spdlog::logger> get_logger() { return spdlog::get(_name); }

protected:
	types::comp_type _type;
	types::comp_status _status;

private:
	string _name;
	iprofile* _profile;
	std::shared_ptr<spdlog::logger> _log;


};

} /* namespace interface */

typedef interface::icomponent_base*(*create_component)(void);
typedef void(*destroy_component)(void);

} /* namespace plunframework */

#endif /* INCLUDE_CORE_ICOMPONENT_BASE_HPP_ */
