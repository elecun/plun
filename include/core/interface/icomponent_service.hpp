/*
 * icomponent_service.hpp
 *
 *  Created on: 2015. 3. 1.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Service Component Interface
 */

#ifndef INCLUDE_CORE_ICOMPONENT_SERVICE_HPP_
#define INCLUDE_CORE_ICOMPONENT_SERVICE_HPP_

#include "icomponent_runnable.hpp"

namespace plunframework {
namespace interface {

class icomponent_service : public icomponent_runnable
{
public:
	icomponent_service(const char* component_name)
	:icomponent_runnable(component_name, types::comp_type::SERVICE) { }
};

}
}


#endif /* INCLUDE_CORE_ICOMPONENT_SERVICE_HPP_ */
