/*
 * component_manager.cpp
 *
 *  Created on: 2015. 3. 2.
 *      Author: byunghun hwang<bhhwang@nsynapse.com>
 *
 *      !!!Note, This code must be optimized. don't forget it.
 */

#include "component_driver.hpp"
#include "./interface/icomponent_base.hpp"
#include "component_manager.hpp"
#include "./interface/icomponent_runnable.hpp"

#include "container.hpp"
#include "../plunframework.hpp"
#include <boost/tokenizer.hpp>

namespace plunframework {
namespace manager {

component_manager* component_manager::_instance = nullptr;

component_manager* component_manager::get()
{
	if(!_instance)
		_instance = new component_manager;
	return _instance;
}

types::return_type component_manager::install(const char* component_name,  int argc, char** argv)
{
	container::component_container* comp_container = container::component_container::get();
	if(!comp_container->exist(component_name))
	{
		if(comp_container->add(component_name, new driver::component_driver(component_name))==types::return_type::SUCCESS)
		{
			error::errorcode e;
			interface::icomponent_base* pComponent = comp_container->get_component(component_name);
			if(pComponent->get_type()!=types::comp_type::GUI) {
				if(pComponent->setup(e))
				{
					string pub_topic = pComponent->get_profile()->get(profile::section::info, "pub").asString("undefined");
					string sub_topic = pComponent->get_profile()->get(profile::section::info, "sub").asString("undefined");

					typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
					boost::char_separator<char> sep{", "};
					tokenizer in{sub_topic, sep};
					for(auto& topic:in){
						if(broker::component_broker::get()->register_topic(pComponent, topic.c_str()))
							spdlog::get("main_thread")->info("Registered Subscribe Topic of [{}] : {}", component_name, topic);
					}

					broker::component_broker::get()->register_topic(pComponent, pub_topic.c_str());

					spdlog::get("main_thread")->info("Registered Publish Topic of [{}] : {}", component_name, pub_topic);
					spdlog::get("main_thread")->info("Installed : {}", component_name);
					return types::return_type::SUCCESS;
				}
			}
			else {
				if(pComponent->setup(e, argc, argv))
				{
					string pub_topic = pComponent->get_profile()->get(profile::section::info, "pub").asString("undefined");
					string sub_topic = pComponent->get_profile()->get(profile::section::info, "sub").asString("undefined");

					typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
					boost::char_separator<char> sep{", "};
					tokenizer in{sub_topic, sep};
					for(auto& topic:in){
						if(broker::component_broker::get()->register_topic(pComponent, topic.c_str()))
							spdlog::get("main_thread")->info("Registered Subscribe Topic of [{}] : {}",component_name, topic);
					}
					broker::component_broker::get()->register_topic(pComponent, pub_topic.c_str());

					spdlog::get("main_thread")->info("Registered Publish Topic of [{}] : {}",component_name, pub_topic);
					spdlog::get("main_thread")->info("GUI Installed : {}", component_name);
					return types::return_type::SUCCESS;
				}
			}
		}


		comp_container->remove(component_name);
		spdlog::get("main_thread")->error("{} component cannot be installed.", component_name);
		return types::return_type::FAIL;

	}
	else
	{
		spdlog::get("main_thread")->warn("{} component is already installed. So, it cannot be re-installed.", component_name);
		return types::return_type::EXIST;
	}
}

types::return_type component_manager::uninstall(const char* component_name)
{
	if(container::component_container::get()->exist(component_name))
	{
		container::component_container* ct = container::component_container::get();
		types::comp_type type = ct->get_component(component_name)->get_type();
		if(type==types::comp_type::APP || type==types::comp_type::SERVICE)
		{
			interface::icomponent_runnable* comp = dynamic_cast<interface::icomponent_runnable*>(ct->get_component(component_name));
			error::errorcode e;
			comp->stop(e);
		}

		ct->remove(component_name);
		spdlog::get("main_thread")->info("Uninstalled {} component", component_name);
		return types::return_type::SUCCESS;
	}
	else
	{
		spdlog::get("main_thread")->error("{} component does not exist.", component_name);
		return types::return_type::FAIL;
	}
}

types::return_type component_manager::run(const char* component_name)
{
	if(container::component_container::get()->exist(component_name))
	{
		container::component_container::get()->get_driver(component_name)->run();
		return types::return_type::SUCCESS;
	}
	else
		return types::return_type::FAIL;
}


void component_manager::run_all()
{
	vector<string> comp_list;
	container::component_container::get()->get_list(comp_list);

	for(auto& itr:comp_list)
		container::component_container::get()->get_driver(itr.c_str())->run();
}

types::return_type component_manager::stop(const char* component_name)
{
	if(container::component_container::get()->exist(component_name))
	{
		container::component_container::get()->get_driver(component_name)->stop();
		spdlog::get("main_thread")->info("Stopped {} component", component_name);
		return types::return_type::SUCCESS;
	}
	else
		return types::return_type::FAIL;
}

void component_manager::stop_all()
{
	vector<string> comp_list;
	container::component_container::get()->get_list(comp_list);

	for(auto& itr:comp_list) {
		container::component_container::get()->get_driver(itr.c_str())->stop();
		spdlog::get("main_thread")->info("Stopped {} component", itr);
	}
}


bool component_manager::is_installed(const char* component_name)
{
	return container::component_container::get()->exist(component_name);
}

void component_manager::destroy()
{
	this->stop_all();
	container::component_container::get()->clear();

	if(_instance)
		delete _instance;

}

int component_manager::count()
{
	return container::component_container::get()->get_size();
}

driver::component_driver* component_manager::get_driver(const char* component_name)
{
	if(container::component_container::get()->exist(component_name))
		return container::component_container::get()->get_driver(component_name);
	else
		return nullptr;
}


} /* namespace component manager */
} /* namespace plunframework */
