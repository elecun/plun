/*
 * typedef.hpp
 *
 *  Created on: 2015. 3. 2.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Type Definition
 */

#ifndef INCLUDE_CORE_TYPEDEF_HPP_
#define INCLUDE_CORE_TYPEDEF_HPP_


#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

using namespace boost;

namespace plunframework {

namespace process {
	typedef boost::shared_ptr<boost::thread> task;

	#define task_register(fnptr)	boost::thread(boost::bind(fnptr, this))
	#define create_task(fnptr)	boost::shared_ptr<boost::thread>(new task_register(&fnptr))
	#define destroy_task(instance)	if(instance){ instance->interrupt(); instance->join(); }
}

namespace types {

	/*
	 * File extension definitions
	 */
	#define COMPONENT_EXT	".comp"
	#define PROFILE_EXT		".xml"


	/* Type Re-definitions */
	//typedef boost::asio::io_service IOService;


	enum class comp_type : unsigned int { ASSIST=101, SERVICE, APP, GUI };
	enum class comp_status : unsigned int { READY = 0, RUNNING, STOPPED };
	enum class return_type : int { EXIST=-1, FAIL=0, SUCCESS=1 };


} /* namespace types */
} /* namespace plunframework */


#endif /* INCLUDE_CORE_TYPEDEF_HPP_ */
