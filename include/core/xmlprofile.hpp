/*
 * xmlprofile.hpp
 *
 *  Created on: 2015. 3. 3.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      XML Profile
 */

#ifndef INCLUDE_CORE_XMLPROFILE_HPP_
#define INCLUDE_CORE_XMLPROFILE_HPP_

#include <tinyxml2.h>
#include <string>
#include "./interface/iprofile.hpp"

using namespace tinyxml2;
using namespace std;

namespace plunframework {
namespace profile {

class xmlprofile : public interface::iprofile {
public:
	xmlprofile();
	virtual ~xmlprofile();

private:
	bool load(const char* filepath);
	profile::type get(profile::section section, const char* element);
	bool update(profile::section section, const char* element, const char* value);
	bool save();

	void error(int code);

	string toString(profile::section section);

private:
	XMLDocument _doc;
	bool _loaded = false;
	string _filepath;
};

}
} /* namespace plunframework */

#endif /* INCLUDE_CORE_XMLPROFILE_HPP_ */
