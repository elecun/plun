/*
 * component_broker.hpp
 *
 *  Created on: 2015. 3. 14.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Component Pub/Sub Broker (for lazy connection)
 *
 *      Application component
 */

#ifndef INCLUDE_CORE_COMPONENT_BROKER_HPP_
#define INCLUDE_CORE_COMPONENT_BROKER_HPP_

#include "component_manager.hpp"
#include "./interface/icomponent_app.hpp"
#include "./interface/icomponent_service.hpp"
#include <boost/unordered_map.hpp>
#include <string>
#include <tuple>
#include <map>
#include "message.hpp"

using namespace std;

namespace plunframework {
namespace broker {

//<component name, topic>
//typedef boost::unordered_map<string, message::topic> topic_map;
typedef std::multimap<string, string> topic_map;	//allow repetition <topic, component name>

class component_broker {

public:
	static component_broker* get();
	void destroy();

	bool register_topic(interface::icomponent_base* comp, const char* topic) {
		bool success = false;
		if(_instance) {
			_topic_map.insert(topic_map::value_type(topic, comp->get_name()));
			success = true;
		}

		return success;
	}

	/*
	 * [Tech Note] implementation of a non-specialized template must be visible!!!
	 * message should not be published to the publisher.
	 */

	template<typename... Args>
	error::errorcode publish(interface::icomponent_base* component, const char* to_topic, const char* api, const Args&... args) {
		error::errorcode code = error::errorcode::PLUN_NO_ERROR;
		if(_instance)
		{
			auto range = _topic_map.equal_range(to_topic);
			for(topic_map::iterator itr = range.first; itr!=range.second; ++itr) {
				if(itr->second.compare(component->get_name())!=0) {
					manager::component_manager::get()->get_driver(itr->second.c_str())->request(api, args...);
				}
			}
		}

		return code;
	}

	template<typename... Args>
	error::errorcode publish(const char* component_name, const char* to_topic, const char* api, const Args&... args) {
		error::errorcode code = error::errorcode::PLUN_NO_ERROR;
		if(_instance) {
			auto range = _topic_map.equal_range(to_topic);
			for(topic_map::iterator itr = range.first; itr!=range.second; ++itr) {
				if(itr->second.compare(component_name)!=0) {
					manager::component_manager::get()->get_driver(itr->second.c_str())->request(api, args...);
				}
			}
		}

		return code;
	}

private:
	static component_broker* _instance;
	topic_map	_topic_map;
};

} /* namespace broker */
} /* namespace plunframework */


#endif /* INCLUDE_CORE_COMPONENT_BROKER_HPP_ */
