/*
 * errorcode.cpp
 *
 *  Created on: 2015. 3. 4.
 *      Author: hwang
 */

#include "errorcode.hpp"
#include "../extra/spdlog/spdlog.h"

namespace plunframework {
namespace error {

void print_error(errorcode& e)
{
	switch(e)
	{
	case errorcode::PLUN_NO_ERROR: break;
	case errorcode::PLUN_NULL_PTR: spdlog::get("main_thread")->error("Cannot access(null pointer) the component"); break;
	case errorcode::PLUN_UNKNOWN_ERROR: spdlog::get("main_thread")->error("Unknown or Undefined Error"); break;
	case errorcode::PLUN_COMPONENT_SETUP_FAIL: 	spdlog::get("main_thread")->error("Failed Setting up component"); break;
	case errorcode::PLUN_BROKER_NOT_REGISTERED: break;
	case errorcode::PLUN_UNPACK_FAIL: spdlog::get("main_thread")->error("Failed message unpacking"); break;
	case errorcode::PLUN_RUN_FAIL: break;
	case errorcode::RUN_STOP_FAIL: break;
	}
}

} /* namespace error */
} /* namespace plunframework */
