/*
 * component_driver.cpp
 *
 *  Created on: 2015. 3. 2.
 *      Author: hwang
 */

#include "component_driver.hpp"
#include <dlfcn.h>
#include "xmlprofile.hpp"
#include "./interface/icomponent_runnable.hpp"
#include "../plunframework.hpp"

namespace plunframework {
namespace driver {

component_driver::component_driver(const char* component_name)
{
	if(this->load(component_name))
	{
		spdlog::get("main_thread")->info("Loaded {} component", component_name);

		string profile_path = "./"+string(component_name)+PROFILE_EXT;
		if(!_ptr_component->set_profile(new profile::xmlprofile, profile_path.c_str()))
			this->unload();
	}
	else
		spdlog::get("main_thread")->error("Cannot load {}", component_name);
}

component_driver::~component_driver()
{
	this->unload();
}

bool component_driver::load(const char* component_name)
{
	string comp_path = "./"+string(component_name)+COMPONENT_EXT;

	_handle = dlopen(comp_path.c_str(), RTLD_LAZY|RTLD_GLOBAL);

	if(!_handle)
	{
		spdlog::get("main_thread")->error("{}", dlerror());
		return false;
	}
	else
	{
		create_component pfcreate = (create_component)dlsym(_handle, "create");
		if(!pfcreate)
		{
			spdlog::get("main_thread")->error("{}",dlerror());

			dlclose(_handle);
			_handle = nullptr;

			return false;
		}
		else
		{
			_ptr_component = pfcreate();
			return true;
		}
	}

}

void component_driver::unload()
{
	if(_ptr_component)
	{
		string comp_name = _ptr_component->get_name();

		destroy_component pfdestroy = (destroy_component)dlsym(_handle, "destroy");

		destroy_task(_request_task);

		if(_ptr_component->get_type()==types::comp_type::APP ||
			_ptr_component->get_type()==types::comp_type::SERVICE )
			destroy_task(_loop_task);

		if(pfdestroy)
			pfdestroy();

		spdlog::get("main_thread")->info("Unloaded {} component ", comp_name);

		_ptr_component = nullptr;
	}

	if(_handle)
	{
		dlclose(_handle);
		_handle = nullptr;
	}
}

void component_driver::run()
{
	error::errorcode e = error::errorcode::PLUN_NO_ERROR;
	if(_ptr_component)
	{
		if(!_request_task)
			_request_task = create_task(component_driver::request_process);

		if(!_loop_task)
			_loop_task = create_task(component_driver::loop);

		_ptr_component->run(e);
	}
	else
		e = error::errorcode::PLUN_NULL_PTR;

	print_error(e);
}

bool component_driver::loop()
{
	bool ret = false;
	if(_ptr_component)
	{
		while(1)
		{
			try {
				if(_ptr_component && (_ptr_component->get_type()!=types::comp_type::ASSIST))
				{
					interface::icomponent_runnable* pComp = dynamic_cast<interface::icomponent_runnable*>(_ptr_component);
					if(!pComp->loop())
						break;
					else
						ret = true;
				};

				boost::this_thread::sleep(posix_time::milliseconds(1));
			} catch(thread_interrupted&)
			{
				break;
			}
		}
	}

	return ret;
}

void component_driver::stop()
{
	error::errorcode e = error::errorcode::PLUN_NO_ERROR;
	if(_ptr_component)
		_ptr_component->stop(e);
	else
		e = error::errorcode::PLUN_NULL_PTR;

	print_error(e);

	destroy_task(_request_task);
	destroy_task(_loop_task);
}

void component_driver::request_process()
{
	if(_ptr_component)
	{
		while(1)
		{
			try
			{
				boost::mutex::scoped_lock lock(_mutex);

				while(_request_box.empty())
					_condition.wait(lock);

				while(!_request_box.empty()) {
					_ptr_component->request(&_request_box.front());
					_request_box.pop();
				}

				boost::this_thread::sleep(posix_time::milliseconds(0));
			}
			catch(thread_interrupted&)
			{
				break;
			}
		}
	}
}


} /* namespace driver */
} /* namespace plunframework */
