/*
 * version.hpp
 *
 *  Created on: 2015. 3. 2.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Version
 */

#ifndef INCLUDE_CORE_VERSION_HPP_
#define INCLUDE_CORE_VERSION_HPP_


namespace plunframework
{


#define PLUN_VERSION_MAJOR	0
#define PLUN_VERSION_MINOR	0
#define PLUN_VERSION_REV	1

#define PLUN_STR(x) #x
#define PLUN_VERSION_SET(major, minor, rev) PLUN_STR(major) "." PLUN_STR(minor) "." PLUN_STR(rev)
#define PLUN_VERSION PLUN_VERSION_SET(PLUN_VERSION_MAJOR, PLUN_VERSION_MINOR, PLUN_VERSION_REV)

}


#endif /* INCLUDE_CORE_VERSION_HPP_ */
