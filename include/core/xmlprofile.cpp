/*
 * xmlprofile.cpp
 *
 *  Created on: 2015. 3. 3.
 *      Author: hwang
 */

#include "xmlprofile.hpp"
#include "../extra/spdlog/spdlog.h"

namespace plunframework {
namespace profile {

xmlprofile::xmlprofile()
{
	// TODO Auto-generated constructor stub

}

xmlprofile::~xmlprofile() {
}

bool xmlprofile::load(const char* filepath)
{
	_filepath = filepath;
	int result = _doc.LoadFile(filepath);

	if(result==XML_SUCCESS)
		_loaded = true;
	else
	{
		this->error(result);
		_loaded = false;
	}

	return _loaded;
}

void xmlprofile::error(int code)
{
	switch(code)
	{
	case	XML_NO_ATTRIBUTE: spdlog::get("main_thread")->error("No Attribute"); break;
	case	XML_WRONG_ATTRIBUTE_TYPE: spdlog::get("main_thread")->error("Wrong attribute type");	break;
	case	XML_ERROR_FILE_NOT_FOUND: spdlog::get("main_thread")->error("Cannot found profile");	break;
	case	XML_ERROR_FILE_COULD_NOT_BE_OPENED: spdlog::get("main_thread")->error("Profile could not be loaded");	break;
	case	XML_ERROR_ELEMENT_MISMATCH: spdlog::get("main_thread")->error("Element mismatch");	break;
	case	XML_ERROR_PARSING_ELEMENT: spdlog::get("main_thread")->error("Parsing element");	break;
	case	XML_ERROR_PARSING_ATTRIBUTE: spdlog::get("main_thread")->error("Parsing attribute");	break;
	case	XML_ERROR_IDENTIFYING_TAG: spdlog::get("main_thread")->error("Identifying tag");	break;
	case	XML_ERROR_PARSING_TEXT: spdlog::get("main_thread")->error("Parsing text");	break;
	case	XML_ERROR_PARSING_CDATA: spdlog::get("main_thread")->error("Parsing CDATA");	break;
	case	XML_ERROR_PARSING_COMMENT: spdlog::get("main_thread")->error("Parsing comment");	break;
	case	XML_ERROR_PARSING_DECLARATION: spdlog::get("main_thread")->error("Parsing declaration");	break;
	case	XML_ERROR_PARSING_UNKNOWN: spdlog::get("main_thread")->error("Parsing unknown");	break;
	case	XML_ERROR_EMPTY_DOCUMENT: spdlog::get("main_thread")->error("Empty document");	break;
	case	XML_ERROR_MISMATCHED_ELEMENT: spdlog::get("main_thread")->error("Mismatched element");	break;
	case	XML_ERROR_PARSING: spdlog::get("main_thread")->error("Parsing Error");	break;
	}
}

string xmlprofile::toString(profile::section section)
{
	string section_name = "";

	switch(section)
	{
	case profile::section::info:		section_name = "info"; break;
	case profile::section::property: section_name = "property";	break;
	case profile::section::resource: section_name = "resource";	break;
	default:	{ section_name.clear();}
	}

	return section_name;
}


profile::type xmlprofile::get(profile::section section, const char* element)
{
	profile::type result;
	string section_name = "";
	bool exist = false;

	switch(section)
	{
	case profile::section::info:		section_name = "info"; exist = true;	break;
	case profile::section::property: section_name = "property";	exist = true;	break;
	case profile::section::resource: section_name = "resource";	exist = true;	break;
	default:	{ section_name = "unknown"; exist = false; }
	}


	if(_loaded && exist && _doc.FirstChildElement(section_name.c_str())!=0)
	{
		if(_doc.FirstChildElement(section_name.c_str())->FirstChildElement(element)!=0)
		{
			XMLNode* _node = _doc.FirstChildElement(section_name.c_str())->FirstChildElement(element)->FirstChild();
			if(_node)
				set(result, _node->ToText()->Value());
			else
				spdlog::get("main_thread")->error("Does not exist element value");
		}
		else
			spdlog::get("main_thread")->error("{} element in {} does not exist.", element, section_name);
	}
	else
		spdlog::get("main_thread")->error("Cannot find {} in the profile", section_name);

	return result;
}

bool xmlprofile::update(profile::section section, const char* element, const char* value)
{
	bool exist = false;

	string section_name = "";
	switch(section)
	{
	case profile::section::property: section_name = "property"; exist=true; break;
	default: { section_name="unknown"; exist=false; }
	}

	if(_loaded && exist && _doc.FirstChildElement(section_name.c_str())!=0)
	{
		if(_doc.FirstChildElement(section_name.c_str())->FirstChildElement(element)!=0)
		{
			XMLNode* pNode = _doc.FirstChildElement(section_name.c_str())->FirstChildElement(element)->FirstChild();
			if(pNode){
				pNode->SetValue(value);
				return true;
			}
		}
		else
			spdlog::get("main_thread")->error("{} element in {} does not exist.", element, section_name);
	}
	else
		spdlog::get("main_thread")->error("Cannot find {} in the profile", section_name);

	return false;
}

bool xmlprofile::save()
{
	if(_loaded) {
		_doc.SaveFile(_filepath.c_str());
		return true;
	}

	return false;
}

} /* namespace profile */
} /* namespace plunframework */
