/*
 * container.hpp
 *
 *  Created on: 2014. 12. 9.
 *      Author: Byunghun Hwang <bhhwang@nsynapse.com>
 *
 *      Component Container
 */

#ifndef CORE_CONTAINER_HPP_
#define CORE_CONTAINER_HPP_

#include <boost/unordered_map.hpp>
#include <string>
#include <vector>
#include "./interface/icomponent_base.hpp"

using namespace std;
using namespace boost;

namespace plunframework {
namespace container {

typedef boost::unordered_map<string, driver::component_driver*> container;

class component_container
{
	friend class manager::component_manager;

private:
	static component_container* get()
	{
		if(!_instance)
			_instance = new component_container;
		return _instance;
	}

	int get_size() { return _component_container.size(); }
	bool empty() { return _component_container.size()!=0; }

	driver::component_driver* get_driver(const char* component_name)
	{
		container::iterator itr = _component_container.find(component_name);
		if(itr!=_component_container.end())
			return itr->second;
		else
			return nullptr;
	}

	interface::icomponent_base* get_component(const char* component_name)
	{
		container::iterator itr = _component_container.find(component_name);
		if(itr!=_component_container.end())
			return itr->second->get_component();
		else
			return nullptr;
	}

	/*
	 * add component_name in the container
	 */
	types::return_type add(const char* component_name, driver::component_driver* driver)
	{
		container::iterator itr = _component_container.find(component_name);
		if(itr==_component_container.end() && driver->valid())
		{
			_component_container.insert(container::value_type(component_name, driver));
			return types::return_type::SUCCESS;
		}
		else
		{
			delete driver;
			return types::return_type::FAIL;
		}
		return types::return_type::EXIST;
	}

	/*
	 * remove component_name component in the container
	 */
	types::return_type remove(const char* component_name)
	{
		container::iterator itr = _component_container.find(component_name);
		if(itr!=_component_container.end())
		{
			delete itr->second;
			_component_container.erase(itr);
			return types::return_type::SUCCESS;
		}
		return types::return_type::FAIL;
	}

	/*
	 * return true if component_name component is installed in the container
	 */
	bool exist(const char* component_name)
	{
		container::iterator itr = _component_container.find(component_name);
		if(itr!=_component_container.end())
			return true;
		else
			return false;
	}

	/*
	 * (not completed) check component_name component is now available.
	 */
	bool available(const char* component_name)
	{
		container::iterator itr = _component_container.find(component_name);
		return true;
	}

	/*
	 * getting component name list
	 */
	void get_list(vector<string>& list)
	{
		for(auto& itr:_component_container)
			list.push_back(itr.first);
	}

	/*
	 * getting application component list
	 */
	void get_app_list(vector<string>& list)
	{
		for(auto& itr:_component_container)
			if(itr.second->get_component()->get_type()==types::comp_type::APP)
				list.push_back(itr.first);
	}

	/*
	 * getting service component list
	 */
	void get_service_list(vector<string>& list)
	{
		for(auto& itr:_component_container)
			if(itr.second->get_component()->get_type()==types::comp_type::SERVICE)
				list.push_back(itr.first);
	}

	/*
	 * getting assist component list
	 */
	void get_assist_list(vector<string>& list)
	{
		for(auto& itr:_component_container)
			if(itr.second->get_component()->get_type()==types::comp_type::ASSIST)
				list.push_back(itr.first);
	}

	/*
	 * unload all components and clear container
	 */
	void clear()
	{
		for(auto& itr:_component_container)
			delete itr.second;
		_component_container.clear();
	}

private:
	container _component_container;
	static component_container* _instance;
};

component_container* component_container::_instance = nullptr;

} /* namespace container */
} /* namespace plunframework */

#endif /* CORE_CONTAINER_H_ */
