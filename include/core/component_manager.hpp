/*
 * component_manager.hpp
 *
 *  Created on: 2015. 3. 2.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Component Manager
 */

#ifndef INCLUDE_CORE_COMPONENT_MANAGER_HPP_
#define INCLUDE_CORE_COMPONENT_MANAGER_HPP_

#include <string>
#include <mutex>
#include "typedef.hpp"
#include "./component_driver.hpp"

using namespace std;

namespace plunframework {
namespace manager {

class component_manager
{
public:

	static component_manager* get();
	void destroy();

	types::return_type install(const char* component_name, int argc=0, char** argv=nullptr);
	types::return_type uninstall(const char* component_name);

	types::return_type run(const char* component_name);
	void run_all();
	types::return_type stop(const char* component_name);
	void stop_all();

	bool is_installed(const char* component_name);
	int count();

	driver::component_driver* get_driver(const char* component_name);

private:
	static component_manager* _instance;

};

} /* namespace component manager */
} /* namespace plunframework */

#endif /* INCLUDE_CORE_COMPONENT_MANAGER_HPP_ */
