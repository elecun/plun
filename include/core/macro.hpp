/*
 * macro.hpp
 *
 *  Created on: 2015. 3. 6.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      PLUN Macro
 */

#ifndef INCLUDE_CORE_MACRO_HPP_
#define INCLUDE_CORE_MACRO_HPP_

#include "./interface/icomponent_base.hpp"

namespace plunframework {

#define COMPONENT_EXPORT				extern "C" { interface::icomponent_base* create(); void destroy(void); }
#define COMPONENT_INSTANCE(classname)	static classname* _instance = nullptr;
#define COMPONENT_CREATE(classname) 	interface::icomponent_base* create(){ \
											if(_instance==nullptr) _instance = new classname(); \
											return _instance; }
#define COMPONENT_DESTROY 				void destroy(){ \
											if(_instance!=nullptr){ \
												delete _instance; _instance=nullptr; }}
#define COMPONENT(classname) #classname


} /* namespace plunframewokr */



#endif /* INCLUDE_CORE_MACRO_HPP_ */
