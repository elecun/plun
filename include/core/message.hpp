/*
 * message.hpp
 *
 *  Created on: 2015. 3. 24.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Message Frame (create with Msgpack for C++)
 */

#ifndef INCLUDE_CORE_MESSAGE_HPP_
#define INCLUDE_CORE_MESSAGE_HPP_

#include <msgpack.hpp>
#include <string>
#include <vector>
#include <iostream>
#include "errorcode.hpp"

namespace plunframework {
namespace message {

typedef std::vector<char>	message;

typedef struct _topic {
	_topic(std::string t):name(t) { }
	_topic(const char* t):name(t) { }
	std::string get() { return name; }
private:
	std::string name;
} topic;


template <typename T>
inline void pack(message* s, const T& v)
{
	msgpack::sbuffer stream_buffer;
	msgpack::pack(stream_buffer, v);

	s->clear();
	s->assign(stream_buffer.data(),stream_buffer.data()+stream_buffer.size());
}

template <typename T>
inline error::errorcode unpack(T* v, const message& s)
{
	error::errorcode e = error::errorcode::PLUN_NO_ERROR;
	try {
	msgpack::sbuffer stream_buffer;
	msgpack::unpacked _unpacked_msg;

	msgpack::unpack(&_unpacked_msg, s.data(), s.size());
	msgpack::object obj = _unpacked_msg.get();
	obj.convert(v);
	}
	catch(std::exception& err) {
		e = error::errorcode::PLUN_UNPACK_FAIL;
	}

	return e;
}

} /* namespace message */
} /* namespace plunframework */

#endif /* INCLUDE_CORE_MESSAGE_HPP_ */
