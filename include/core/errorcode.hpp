/*
 * errorcode.hpp
 *
 *  Created on: 2015. 3. 4.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      System Errorcode
 */

#ifndef INCLUDE_CORE_ERRORCODE_HPP_
#define INCLUDE_CORE_ERRORCODE_HPP_

namespace plunframework {
namespace error {

enum class errorcode : unsigned long {
	PLUN_NO_ERROR = 1,	/* No error  */
	PLUN_NULL_PTR,			/* nullptr component */

	PLUN_UNPACK_FAIL = 10,
	PLUN_COMPONENT_SETUP_FAIL,
	PLUN_RUN_FAIL,
	RUN_STOP_FAIL,
	PLUN_UNKNOWN_ERROR = 99,

	PLUN_BROKER_NOT_REGISTERED = 110,

};

#ifndef _cplusplus
extern "C" {
#endif

void print_error(errorcode& e);

#ifndef _cplusplus
}
#endif

}
} /* namespace plunframework */

#endif /* INCLUDE_CORE_ERRORCODE_HPP_ */
