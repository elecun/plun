/*
 * localtime.hpp
 *
 *  Created on: 2015. 3. 12.
 *      Author: hwang
 */

#ifndef INCLUDE_UTIL_LOCALTIME_HPP_
#define INCLUDE_UTIL_LOCALTIME_HPP_

#include <ctime>
#include <string>
#include <boost/format.hpp>

using namespace std;
using namespace boost;

namespace plunframework {

struct sTime : public tm
{
	int ctm_sec;
	int ctm_min;
	int ctm_hour;
	int ctm_mday;
	int ctm_mon;
	int ctm_year;
	int ctm_wday;
	int ctm_yday;
	int ctm_isdst;

	sTime* local()
	{
		time_t now = time(0);
		tm* local = localtime(&now);

		ctm_sec = local->tm_sec;
		ctm_min = local->tm_min+1;
		ctm_hour = local->tm_hour;
		ctm_mday = local->tm_mday;
		ctm_mon = local->tm_mon+1;
		ctm_year = local->tm_year+1900;
		ctm_wday = local->tm_wday;
		ctm_yday = local->tm_yday;
		ctm_isdst = local->tm_isdst;

		return this;
	}

	string formatted()
	{
		local();
		string ts = str(format("%1%-%2%-%3% %4%:%5%:%6%")
							%(ctm_year)
							%(ctm_mon)
							%(ctm_mday)
							%(ctm_hour)
							%(ctm_min)
							%(ctm_sec));
		return ts;
	}
};

#ifndef _cplusplus
extern "C" {
#endif

string get_localtime();

#ifndef _cplusplus
}
#endif

class time_counter
{
public:
	time_counter() { set(); }
	virtual ~time_counter() { }

	long get_ms();
	long get_us();
	long get_ns();

private:
	void set();

private:
	struct timespec _time;
};

} /* namespace plunframework */



#endif /* INCLUDE_UTIL_LOCALTIME_HPP_ */
