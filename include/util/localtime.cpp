/*
 * localtime.cpp
 *
 *  Created on: 2015. 3. 2.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 */

#include "localtime.hpp"

namespace plunframework {

string get_localtime()
{
	sTime ctime;
	return ctime.formatted();
}

void time_counter::set()
{
	clock_gettime(CLOCK_REALTIME, &_time);
}

long time_counter::get_ms()
{
	struct timespec _time_after;
	clock_gettime(CLOCK_REALTIME, &_time_after);
	return long(_time_after.tv_nsec/1000000 - _time.tv_nsec/1000000);
}

long time_counter::get_us()
{
	struct timespec _time_after;
	clock_gettime(CLOCK_REALTIME, &_time_after);
	return long(_time_after.tv_nsec/1000 - _time.tv_nsec/1000);
}

long time_counter::get_ns()
{
	struct timespec _time_after;
	clock_gettime(CLOCK_REALTIME, &_time_after);
	return long(_time_after.tv_nsec - _time.tv_nsec);
}

} /* namespace plunframework */
