#!/bin/bash

cd /home/odroid/plun/Debug
make clean
make

cd /home/odroid/plun/components/appYMTHost/Debug
make clean
make

cd /home/odroid/plun/components/filelog/Debug
make clean
make

cd /home/odroid/plun/components/naaflogadapter/Debug
make clean
make

cd /home/odroid/plun/components/naafprotocol/Debug
make clean
make

cd /home/odroid/plun/components/serial/Debug
make clean
make

cd /home/odroid/plun/components/tcpserver/Debug
make clean
make

cd /home/odroid/plun/components/naaftcpadapter/Debug
make clean
make
