/*
 * naafprotocol.hpp
 *
 *  Created on: 2015. 5. 6.
 *      Author:
 *
 *      YMT Nickel Analyzer & Automatic Feeder Protocol
 */

#ifndef NAAFPROTOCOL_HPP_
#define NAAFPROTOCOL_HPP_

#include "../../include/core/interface/icomponent_assist.hpp"
#include <deque>
#include <string>

using namespace std;

namespace plunframework {
namespace component {

class naafprotocol : public interface::icomponent_assist {
public:
	naafprotocol();
	virtual ~naafprotocol();

	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

private:
	void process();
	void print_hex(unsigned char* data, int len);

private:
	deque<unsigned char> _queue;
	string _topic;
};

COMPONENT_EXPORT

}
} /* namespace plunframework */

#endif /* NAAFPROTOCOL_HPP_ */
