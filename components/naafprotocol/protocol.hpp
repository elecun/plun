/*
 * protocol.hpp
 *
 *  Created on: 2015. 5. 18.
 *      Author: byunghun hwang<bhhwang@nsynapse.com>
 *
 *      YMT Nickel Analyzer & Automatic Feeder Protocol
 */

#ifndef PROTOCOL_HPP_
#define PROTOCOL_HPP_

#include <cstring>
#include <iostream>


#define FRAME_HEADER	{0x7b, 0x02}
#define FRAME_TAIL	{0x03, 0x7d}
#define CHECKSUM_BIT	27

//function code
#define FUNCTION_SQ	{0x53, 0x51}
#define FUNCTION_SR	{0x53, 0x52}

//protocol for request
const unsigned char REQUEST_MODE_AUTO		= 0x00;
const unsigned char REQUEST_MODE_MANUAL	= 0x01;
const unsigned char REQUEST_PC_STATUS		= 0X00;

//request error code
const unsigned int	REQUEST_ERROR_NULL					= 0x00000000;
const unsigned int	REQUEST_ERROR_EPUMP_SPEED			= 0x00000001;		//E PUMP ERROR
const unsigned int	REQUEST_ERROR_DPUMP 					= 0x00000002;		//D PUMP ERROR
const unsigned int	REQUEST_ERROR_CPUMP 					= 0x00000004;		//C PUMP ERROR
const unsigned int	REQUEST_ERROR_BPUMP 					= 0x00000008;		//B PUMP ERROR
const unsigned int	REQUEST_ERROR_APUMP 					= 0x00000010;		//A PUMP ERROR
const unsigned int	REQUEST_ERROR_SPUMP 					= 0x00000020;		//S PUMP ERROR
const unsigned int	REQUEST_ERROR_SPUMP_SPEED		 	= 0x00000040;		//S PUMP ERROR SPEED

const unsigned int	REQUEST_ERROR_MODE_ERROR				= 0x00000100;	//mode error
const unsigned int	REQUEST_ERROR_AC_DRIVE_COM_ERROR	= 0x00000200;	//ac motor drive com error
const unsigned int	REQUEST_ERROR_UV_AMP_COM_ERROR		= 0x00000400;	//ymt uv amp com error
const unsigned int	REQUEST_ERROR_CUWIN_COM_ERROR		= 0x00000800; //cuwin com error
const unsigned int	REQUEST_ERROR_CUWIN_BCC_ERROR		= 0x00001000;	//cuwin bcc error
const unsigned int	REQUEST_ERROR_CUWIN_LEN_ERROR		= 0x00002000;	//cuwin len error
const unsigned int	REQUEST_ERROR_TEMP_MIN_ERROR		= 0x00004000;	//temperature value min excursion error
const unsigned int	REQUEST_ERROR_TEMP_MAX_ERROR		= 0x00008000;	//temperature value max excursion error

const unsigned int	REQUEST_ERROR_MB_COM_ERROR			= 0x00010000;	//main board com error
const unsigned int	REQUEST_ERROR_MB_BCC_ERROR			= 0x00020000;	//main board bcc error
const unsigned int	REQUEST_ERROR_MB_LEN_ERROR			= 0x00040000;	//main board len error
const unsigned int	REQUEST_ERROR_MTO_ERROR				= 0x00080000;	//?
const unsigned int	REQUEST_ERROR_PH_MIN_ERROR			= 0x00100000;	//ph value min excursion error
const unsigned int	REQUEST_ERROR_PH_MAX_ERROR			= 0x00200000;	//ph value max excursion error
const unsigned int	REQUEST_ERROR_NICKEL_MIN_ERROR		= 0x00400000;	//nickel min excursion error
const unsigned int	REQUEST_ERROR_NICKEL_MAX_ERROR		= 0x00800000;	//nickel max excursion error

const unsigned char	REQUEST_SPUMP_SPEED[10]				= { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a};
const unsigned char	EPUMP						= 0x01;
const unsigned char	DPUMP						= 0x02;
const unsigned char	CPUMP						= 0x04;
const unsigned char	BPUMP						= 0x08;
const unsigned char	APUMP						= 0x10;
const unsigned char	SPUMP						= 0x20;


//protocol for response

const int PACKET_REQUEST_LENGTH = 23;
struct PACKET_REQUEST {
	unsigned char header[2] = FRAME_HEADER;
	unsigned char len[2] = { 0x00, 0x11 };
	unsigned char func[2] = FUNCTION_SQ;
	unsigned char mode = 0x00;
	unsigned char status = 0x00;
	unsigned char error[3] = { 0x00, };
	unsigned char spump_speed = 0x00;
	unsigned char pump = 0x00;
	unsigned char mto[2] = { 0x00, };
	unsigned char ni_gram[2] = { 0x00, };
	unsigned char ph[2] = { 0x00, };
	unsigned char temp = 0x00;
	unsigned char bcc = 0x00;
	unsigned char footer[2] = FRAME_TAIL;

	unsigned char* set_mode(unsigned char mode) { this->mode = mode; return &this->mode; }
	unsigned char* set_status(unsigned char status) { this->status = status; return &this->status; }
	unsigned char* set_error(unsigned int error) {
		this->error[0] = (error>>16)&0xff;
		this->error[1] = (error>>8)&0xff;
		this->error[2] = error&0xff;
		return this->error;
	}
	unsigned char* set_spump_speed(unsigned char speed) { this->spump_speed = speed; return &this->spump_speed; }
	unsigned char* set_pump(unsigned char pump) { this->pump = pump; return &this->pump; }
	unsigned char* set_mto(double value) {
		this->mto[0] = ((unsigned int)(value*100)>>8)&0xff;
		this->mto[1] = ((unsigned int)(value*100))&0xff;
		return this->mto;
	}
	unsigned char* set_ni_gram(double value) {
		this->ni_gram[0] = ((unsigned int)(value*100)>>8)&0xff;
		this->ni_gram[1] = ((unsigned int)(value*100))&0xff;
		return this->ni_gram;
	}
	unsigned char* set_ph(double value) {
		this->ph[0] = ((unsigned int)(value*100)>>8)&0xff;
		this->ph[1] = ((unsigned int)(value*100))&0xff;
		return this->ph;
	}
	unsigned char* set_temp(unsigned char temp) { this->temp = temp; return &this->temp; }
	PACKET_REQUEST* get_packet() {
		this->bcc = 0x00;
		for(int idx=2;idx<20;idx++)
			this->bcc ^= *((unsigned char*)this+idx);
		return this;
	}
};

const int PACKET_RESPONSE_LENGTH = 31;
struct PACKET_RESPONSE {
	unsigned char header[2] = FRAME_HEADER;
	unsigned char len[2] = { 0x00, 0x19 };
	unsigned char func[2] = FUNCTION_SR;
	unsigned char mode = 0x00;
	unsigned char status = 0x00;
	unsigned char error[3] = { 0x00, };
	unsigned char spump_speed = 0x00;
	unsigned char pump = 0x00;
	unsigned char ni_sensor_dc[2] = { 0x00, };
	unsigned char ni_sensor_temp[2] = { 0x00, };
	unsigned char ph_sensor[2] = { 0x00, };
	unsigned char ph_sensor_temp[2] = { 0x00, };
	unsigned char mto[2] = { 0x00, };
	unsigned char ni_gram[2];
	unsigned char ph[2] = { 0x00, };
	unsigned char temp = 0x00;
	unsigned char bcc = 0x00;
	unsigned char footer[2] = FRAME_TAIL;

	unsigned int get_spump_speed() { return (unsigned int)spump_speed; }
	unsigned int get_error() { return 0; }
	double get_ni_sensor_dc() { return (double)((unsigned int)this->ni_sensor_dc[0]*256+(unsigned int)this->ni_sensor_dc[1])/1000; }
	double get_ni_sensor_temp() { return (double)((unsigned int)this->ni_sensor_temp[0]*256+(unsigned int)this->ni_sensor_temp[1])/100; }
	double get_ph_sensor() { return (double)((unsigned int)this->ph_sensor[0]*256+(unsigned int)this->ph_sensor[1])/1000; }
	double get_ph_sensor_temp() { return (double)((unsigned int)this->ph_sensor_temp[0]*256+(unsigned int)this->ph_sensor_temp[1])/100; }
	double get_mto() { return (double)((unsigned int)this->mto[0]*256+(unsigned int)this->mto[1])/100; }
	double get_ni_gram() { return (double)((unsigned int)this->ni_gram[0]*256+(unsigned int)this->ni_gram[1])/100; }
	double get_ph() { return (double)((unsigned int)this->ph[0]*256+(unsigned int)this->ph[1])/100; }
	unsigned int get_temperature() { return (unsigned int)this->temp; }
	bool is_valid() {
		unsigned char chksum = 0x00;
		for(int idx=2;idx<28;idx++)
			chksum ^= *((unsigned char*)this+idx);

		if(chksum==this->bcc)
			return true;

		return false;
	}


};


#endif /* PROTOCOL_HPP_ */
