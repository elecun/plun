/*
 * naafprotocol.cpp
 *
 *  Created on: 2015. 5. 6.
 *      Author: byunghun hwang<bhhwang@nsynapse.com>
 */

#include "naafprotocol.hpp"
#include "protocol.hpp"
#include "../../include/core/component_broker.hpp"
#include <vector>
#include <algorithm>

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(naafprotocol)
COMPONENT_CREATE(naafprotocol)
COMPONENT_DESTROY

naafprotocol::naafprotocol()
:interface::icomponent_assist(COMPONENT(naafprotocol)){
	// TODO Auto-generated constructor stub

}

naafprotocol::~naafprotocol() {
	// TODO Auto-generated destructor stub
}

bool naafprotocol::setup(error::errorcode& e, int argc=0, char** argv=nullptr)
{
	_topic = get_profile()->get(profile::section::info, "pub").asString("protocol/naaf");

	return true;
}

bool naafprotocol::run(error::errorcode& e)
{

	return true;
}

bool naafprotocol::stop(error::errorcode& e)
{
	return true;
}

void naafprotocol::request(message::message* msg)
{
	std::tuple<string, vector<unsigned char>> unpacked_data;
	if(message::unpack(&unpacked_data, *msg)==error::errorcode::PLUN_NO_ERROR) {
		if(std::get<0>(unpacked_data)=="raw") {
			vector<unsigned char> data = std::get<1>(unpacked_data);

			for(auto& c:data)
				_queue.push_back(c);

			process();

		}
		else {
			get_logger()->error("Undefined Message was received.");
		}
	}

}

void naafprotocol::process()
{
	while(_queue.size()>=PACKET_RESPONSE_LENGTH)
	{
		//header check
		if(_queue.at(0)==0x7b && _queue.at(1)==0x02)
		{
			//tail check
			if(_queue.at(PACKET_RESPONSE_LENGTH-2)==0x03 && _queue.at(PACKET_RESPONSE_LENGTH-1)==0x7d)
			{
				//checksum check
				unsigned char chksum = 0x00;
				for(int i=2;i<PACKET_RESPONSE_LENGTH-3;i++)
					chksum ^= _queue.at(i);

				if(chksum==_queue.at(PACKET_RESPONSE_LENGTH-3))
				{
					vector<unsigned char> data;
					std::copy(_queue.begin(), _queue.end(), std::back_inserter(data));
					broker::component_broker::get()->publish(this, _topic.c_str(), "protocol", data);
					get_logger()->info("Published message via {}",_topic);

				}
				else
					get_logger()->error("Invalid Packet (Checksum Error)");


				for(int i=0;i<PACKET_RESPONSE_LENGTH;i++)
					_queue.pop_front();
			}
			else
			{
				//get_logger()->error("Invalid Packet({})",_queue.size());
				while(_queue.front()!=0x7d)
				{
					if(_queue.empty()) break;
					_queue.pop_front();
				}
			}

		}
		else
		{
			_queue.pop_front();
			if(_queue.empty()) break;
		}
	}
}

void naafprotocol::print_hex(unsigned char* data, int len)
{
	string res="";
	char const hex[16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	for(int i=0;i<len; ++i)
	{
		const char c = data[i];
		res.append(&hex[(c&0xf0)>>4], 1);
		res.append(&hex[c&0x0f],1);
		res.append(" ");
	}

	get_logger()->info("Data : {}", res);

}



}
} /* namespace plunframework */
