/*
 * appwindow.hpp
 *
 *  Created on: 2015. 5. 16.
 *      Author: hwang-linux
 */

#ifndef APPWINDOW_HPP_
#define APPWINDOW_HPP_

#include <gtkmm-3.0/gtkmm.h>
#include <cairomm-1.0/cairomm/cairomm.h>
#include <glibmm-2.4/glibmm.h>

#include <gtkmm-3.0/gtkmm/window.h>
#include <gtkmm-3.0/gtkmm/application.h>
#include <gtkmm-3.0/gtkmm/builder.h>
#include <glibmm-2.4/glibmm/refptr.h>
#include <gtkmm-3.0/gtkmm/switch.h>
#include <gtkmm-3.0/gtkmm/levelbar.h>
#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/label.h>
#include <sigc++-2.0/sigc++/sigc++.h>

#include <string>
#include <map>
#include <fstream>
#include <boost/filesystem.hpp>

#include "gtkGraph.hpp"
#include "protocol.hpp"
#include "../../include/core/interface/iprofile.hpp"

using namespace std;
using namespace Gtk;
using namespace Glib;

/*
 * tab window index
 */
enum FRAME_INDEX {MAIN_FRAME=0, PASSWORD_FRAME, SETTING_FRAME};
enum SETTING_INDEX { MAIN=0, PLATING, OTHERS, PUMP  };
enum GRAPH_INDEX { GRAPH=0, ERROR, EMERGENCY, PWRDOWN };



class appwindow : public Gtk::Window {

	typedef enum { MTO, NI, PH, TEMP } MONITOR_TARGET;

public:
	appwindow(BaseObjectType* obj, const Glib::RefPtr<Gtk::Builder>& glade);
	virtual ~appwindow();

	appwindow* get_window() { return this; }

	void set_profile(plunframework::interface::iprofile* profile);
	void set_topic(string topic) { _topic = topic; }
	void set_property(const char* name, const char* value);
	void set_data(PACKET_RESPONSE* data);
	void set_com_error();

	void log_alarm(const char* text);
	void log_message(const char* text);
	void refresh();

	void init();


private:
	bool on_draw_graph(const Cairo::RefPtr<Cairo::Context>& cr);
	void error_process(PACKET_RESPONSE* data, unsigned int error_code);
	void pump_process(unsigned char pump_code);
	void state_process(unsigned char state_code);
	void mode_process(unsigned char mode_code);

	/*
	 * button event function
	 */

	void btn_init();
	void on_btn_sample_click();
	void on_btn_apump_click();
	void on_btn_bpump_click();
	void on_btn_cpump_click();
	void on_btn_dpump_click();
	void on_btn_epump_click();
	void on_btn_setting_click();
	void on_btn_password_0_click();
	void on_btn_password_1_click();
	void on_btn_password_2_click();
	void on_btn_password_3_click();
	void on_btn_password_4_click();
	void on_btn_password_5_click();
	void on_btn_password_6_click();
	void on_btn_password_7_click();
	void on_btn_password_8_click();
	void on_btn_password_9_click();
	void on_btn_password_back_click();
	void on_btn_password_ok_click();
	void on_btn_password_esc_click();

	void on_btn_setting_exit_click();
	void on_btn_setting_plating_click();
	void on_btn_setting_others_click();
	void on_btn_setting_pump_click();
	void on_btn_setting_mto_click();
	void on_btn_setting_back_click();

	void on_btn_spump_speedup_click();
	void on_btn_spump_speeddown_click();
	void on_btn_change_password_click();

	void on_btn_change_password_esc_click();
	void on_btn_change_password_ok_click();
	void on_btn_change_password_del_click();
	void on_btn_change_password_0_click();
	void on_btn_change_password_1_click();
	void on_btn_change_password_2_click();
	void on_btn_change_password_3_click();
	void on_btn_change_password_4_click();
	void on_btn_change_password_5_click();
	void on_btn_change_password_6_click();
	void on_btn_change_password_7_click();
	void on_btn_change_password_8_click();
	void on_btn_change_password_9_click();

	void on_sw_mode_changed(const Gtk::StateType& style);

	/*
	 * etc
	 */
	void send_request();
	void in_password(const char* pw);

private:
	std::map<string, string> _property;
	Glib::RefPtr<Gtk::Builder> _builder;
	plunframework::interface::iprofile* _profile = nullptr;

	/*
	 * user input
	 */
	/*Gtk::Button* _btn_sample;
	Gtk::Button* _btn_apump;
	Gtk::Button* _btn_bpump;
	Gtk::Button* _btn_cpump;
	Gtk::Button* _btn_dpump;
	Gtk::Button* _btn_epump;*/
	Gtk::Button* _btn_setting;
	Gtk::Switch* _sw_auto_mode;
	Gtk::Label* _sample_text;
	Gtk::Label* _apump_text;
	Gtk::Label* _bpump_text;
	Gtk::Label* _cpump_text;
	Gtk::Label* _dpump_text;
	Gtk::Label* _epump_text;
	Gtk::Image* _apump_icon;
	Gtk::Image* _bpump_icon;
	Gtk::Image* _cpump_icon;
	Gtk::Image* _dpump_icon;
	Gtk::Image* _epump_icon;
	Gtk::Image* _sample_icon;

	/*
	 * setting main menu
	 */
	Gtk::Button* _btn_setting_menu[6];
	Gtk::Notebook* _setting_tab;

	/*
	 * change password dialog
	 */
	Gtk::Button* _btn_change_pw_key[10];
	Gtk::Button* _btn_change_pw_key_esc;
	Gtk::Button* _btn_change_pw_key_ok;
	Gtk::Button* _btn_change_pw_key_del;
	Gtk::Entry* _edit_new_password;
	Gtk::Entry* _edit_password_confirm;

	/*
	 * windows & dialog
	 */
	Gtk::Dialog* _exit_dialog;					//exit program dialog
	Gtk::Dialog* _save_dialog;					//save parameter dialog
	Gtk::Window* _change_pw_dialog;				//change password dialog
	Gtk::Window* _keypad_dialog;				//keypad for entering value


	/*
	 * error led indicator
	 */
	Gtk::Image* _ni_max_error;
	Gtk::Image* _ni_min_error;
	Gtk::Image* _ph_max_error;
	Gtk::Image* _ph_min_error;
	Gtk::Image* _temp_max_error;
	Gtk::Image* _temp_min_error;
	Gtk::Image* _pc_com_error;
	Gtk::Image* _pc_len_error;
	Gtk::Image* _pc_bcc_error;
	Gtk::Image* _cuwin_com_error;
	Gtk::Image* _cuwin_len_error;
	Gtk::Image* _cuwin_bcc_error;
	Gtk::Image* _ni_com_error;
	Gtk::Image* _motor_com_error;
	Gtk::Image* _buzzer_stop_error;
	Gtk::Image* _spump_speed_error;
	Gtk::Image* _spump_error;
	Gtk::Image* _apump_error;
	Gtk::Image* _bpump_error;
	Gtk::Image* _cpump_error;
	Gtk::Image* _dpump_error;
	Gtk::Image* _epump_error;



	Gtk::Button* _btn_spump_speed_up;
	Gtk::Button* _btn_spump_speed_down;

	Gtk::Button* _btn_password_numberpad[10];
	Gtk::Button* _btn_password_back;
	Gtk::Button* _btn_password_ok;
	Gtk::Button* _btn_password_esc;
	Gtk::Entry* _edit_password;

	Gtk::LevelBar* _spump_levelbar;
	Gtk::Label* _spump_speed_label;

	Gtk::Button* _btn_change_password;


	Gtk::DrawingArea* _graph_window;
	Gtk::TextView* _message_window;
	Gtk::TextIter _message_buffer_itr;
	Gtk::TextView* _alarm_window;
	Gtk::TextIter _alarm_buffer_itr;
	Gtk::Notebook* _right_tab;
	Gtk::Notebook* _graph_tab;

	/*
	 * publish data
	 */
	PACKET_REQUEST _request_packet;
	string _topic;

	/*
	 * main monitoring value
	 */
	Gtk::Label* _monitor_mto;
	Gtk::Label* _monitor_ph;
	Gtk::Label* _monitor_current_ph;
	Gtk::Label* _monitor_ni;
	Gtk::Label* _monitor_current_ni;
	Gtk::Label* _monitor_temp;
	Gtk::Label* _monitor_current_temp;
	Gtk::Label* _monitor_ni_sensor;


	gtkGraph* _graph_impl = nullptr;


	/*
	 * log for gui
	 */
	string _filename;
	std::ofstream* _alarm_logfile = nullptr;
};


#endif /* APPWINDOW_HPP_ */
