/*
 * appYMTHost.cpp
 *
 *  Created on: 2015. 5. 16.
 *      Author: hwang-linux
 */

#include "appYMTHost.hpp"
#include "../../include/core/component_manager.hpp"
#include "../../include/core/component_broker.hpp"
#include "protocol.hpp"
#include <boost/filesystem.hpp>
#include "appwindow.hpp"

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(appYMTHost)
COMPONENT_CREATE(appYMTHost)
COMPONENT_DESTROY

appYMTHost::appYMTHost()
:icomponent_gui(COMPONENT(appYMTHost)){

}

appYMTHost::~appYMTHost() {
	// TODO Auto-generated destructor stub
}

bool appYMTHost::setup(error::errorcode& e, int argc=1, char** argv=nullptr)
{
	string gui_file = get_profile()->get(profile::section::property, "gui").asString(this->get_name());

	int arg = 1;	//!! 1 for gtk application with no argument
	_app = Gtk::Application::create(arg, argv, "app.YMTHost");
	_builder = Gtk::Builder::create();

	try
	{
		if(!gui_file.empty()) {
			_builder->add_from_file(gui_file.c_str());
		}
		else
			throw Glib::FileError::FAILED;
	}
	catch(const Glib::FileError& ex)
	{
		get_logger()->error("GUI File Error : {}", ex.what());
		return false;
	}
	catch(const Glib::MarkupError& ex)
	{
		get_logger()->error("GUI Markup Error : {}", ex.what());
		return false;
	}
	catch(const Gtk::BuilderError& ex)
	{
		get_logger()->error("GUI Builder Error : {}", ex.what());
		return false;
	}

	_builder->get_widget_derived("ID_MAIN_WINDOW", _gui_window);
	_gui_window->set_profile(get_profile());

	/*
	 * Read Property
	 */
	_topic = get_profile()->get(profile::section::info, "pub").asString("application/ymthost");

	_gui_window->set_property("pub", _topic.c_str());
	_gui_window->set_property("name", this->get_name());
	_gui_window->set_property("password", get_profile()->get(profile::section::property, "password").asString("000000").c_str());
	_gui_window->set_property("spump_speed", get_profile()->get(profile::section::property, "spump_speed").asString("0").c_str());
	_gui_window->set_property("use_termperature", get_profile()->get(profile::section::property, "use_termperature").asString("0").c_str());
	_gui_window->set_property("use_dpump", get_profile()->get(profile::section::property, "use_dpump").asString("0").c_str());
	_gui_window->set_property("use_epump", get_profile()->get(profile::section::property, "use_epump").asString("0").c_str());


	/*
	 * Create Directory (Job & Alarm)
	 */
	string alarm_dir_prefix = get_profile()->get(profile::section::property, "alaram_log_prefix").asString("./");
	_gui_window->set_property("alaram_log_prefix", alarm_dir_prefix.c_str());

	_gui_window->set_property("reffile_path", get_profile()->get(profile::section::property, "reffile_path").asString("./").c_str());
	_gui_window->set_property("savefile_path", get_profile()->get(profile::section::property, "savefile_path").asString("./").c_str());

	_gui_window->init();

	/*boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
	struct tm system_time = boost::posix_time::to_tm(now);
	string alarm_path = fmt::format("{}/{}-{}-{}",alarm_dir_prefix, (system_time.tm_year+1900), (system_time.tm_mon+1), (system_time.tm_mday));

	boost::filesystem::path dir_alarm(alarm_path);
	boost::filesystem::create_directories(dir_alarm);*/


	return true;
}

bool appYMTHost::run(error::errorcode& e)
{
	//!! gui app. should be run in task
	if(!_run_task)
		_run_task = create_task(component::appYMTHost::run_task);

	return false;
}

bool appYMTHost::stop(error::errorcode& e)
{
	if(_gui_window)
		delete _gui_window;

	destroy_task(_run_task);

	return true;
}

//
void appYMTHost::request(message::message* msg)
{
	//from protocol component
	std::tuple<string, vector<unsigned char>> unpacked_data;
	if(message::unpack(&unpacked_data, *msg)==error::errorcode::PLUN_NO_ERROR) {
		if(std::get<0>(unpacked_data)=="protocol") {
			vector<unsigned char> data = std::get<1>(unpacked_data);

			if(data.size()==PACKET_RESPONSE_LENGTH)
			{
				unsigned char* pdata = &data[0];
				PACKET_RESPONSE* packet = (PACKET_RESPONSE*)pdata;

				boost::mutex::scoped_lock lock(_mutex);
				{
					_gui_window->set_data(packet);

					/*
					 * generate log message
					 */
					boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
					struct tm st = boost::posix_time::to_tm(now);
					string logdata = fmt::format("[{}-{}-{} {}:{}:{}]\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n",(st.tm_year+1900), (st.tm_mon+1), (st.tm_mday), (st.tm_hour), (st.tm_min), (st.tm_sec),
							packet->get_ni_gram(), packet->get_ni_sensor_dc(), packet->get_ph(), packet->get_mto(), 0,0,0,0,0,0,"1111111");

					_gui_window->log_message(logdata.c_str());

					//broker::component_broker::get()->publish(this, _topic.c_str(), "log", logdata);

					_gui_window->refresh();

					_rcv_check = 0;

				}

			}

		}
		else {
			get_logger()->error("Undefined Message was received.");
		}
	}
}

bool appYMTHost::loop()
{
	if(_rcv_check>1)
	{
		_gui_window->set_com_error();
		cout << "com error" << endl;
		_rcv_check = 0;
	}
	_rcv_check++;
	boost::this_thread::sleep(posix_time::milliseconds(1000));
	return true;
}


void appYMTHost::run_task()
{
	if(_gui_window)
		_app->run(*_gui_window);
}

void appYMTHost::print_hex(unsigned char* data, int len)
{
	string res="";
	char const hex[16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	for(int i=0;i<len; ++i)
	{
		const char c = data[i];
		res.append(&hex[(c&0xf0)>>4], 1);
		res.append(&hex[c&0x0f],1);
		res.append(" ");
	}

	get_logger()->info("Received Data : {}", res);

}


} /* namespace component */
} /* namespace plunframework */
