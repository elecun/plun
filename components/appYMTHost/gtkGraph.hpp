/*
 * gtkGraph.hpp
 *
 *  Created on: 2015. 5. 18.
 *      Author: hwang-linux
 */

#ifndef GTKGRAPH_HPP_
#define GTKGRAPH_HPP_

#include <gtkmm-3.0/gtkmm/drawingarea.h>
#include <cairomm-1.0/cairomm/cairomm.h>
#include <cairomm-1.0/cairomm/context.h>
#include <cairomm-1.0/cairomm/refptr.h>
#include <map>
#include <string>
#include <queue>
#include <deque>
#include <iostream>

using namespace std;
using namespace Gtk;
using namespace Glib;
using namespace Cairo;

/*
 * single graph line control class
 */
class Graph
{
public:
	Graph(const char* title, GdkRGBA& rgba, unsigned int limit):_limit(limit) {
		this->_color = rgba;
	}
	~Graph() { }

	void push(double data) {
		_value.push_back(data);
		if(_value.size()>_limit) {
			unsigned int over = _value.size()-_limit;
			for(unsigned int i=0;i<over;i++)
				_value.pop_front();
		}
	}
	void pop_front() {
		if(!_value.empty())
			_value.pop_front();
	}
	double front() {
		if(!_value.empty())
			return _value.front();
		else
			return 0.0;
	}
	double get(unsigned int at) {
		return _value[at];
	}
	GdkRGBA* get_color() { return &_color; }
	unsigned int get_data_size() {
		if(_value.empty())
			return 0;
		else
			return _value.size();
	}
	double get_data_max() {
		double max = 0.0;
		for(auto& data:_value) {
			if(max<data) max=data;
		}
		return max;
	}

private:
	std::string _title;
	GdkRGBA _color;
	std::deque<double> _value;
	unsigned int _limit = 0;
};

class gtkGraph : public Gtk::DrawingArea {
public:
	gtkGraph();
	virtual ~gtkGraph();

	void on_draw(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation);

	void set_data_range(double x_max, double y_max) { _x_max_range =x_max; _y_max_range=y_max;  }
	void add_graph(const char* graph_title, GdkRGBA& color);
	void push_data(const char* graph_title, double data);

private:
	void draw_window_outline(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, double width);
	void draw_graph_outline(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, double width);
	void draw_background(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color);
	void draw_grid(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, double width, int grid);
	void draw_xlabel(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, int grid, const char* text);
	void draw_ylabel(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, int grid, const char* text);
	void draw_text(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, int x, int y, const char* text);
	void draw_graph(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation);

	void draw_window_outline(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color, double width);
	void draw_graph_outline(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color, double width);
	void draw_background(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color);
	void draw_grid(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color, double width, int grid);
	void draw_xlabel(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, double screen_width, double screen_height, GdkRGBA& color, int grid, const char* text);
	void draw_ylabel(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, double screen_width, double screen_height, GdkRGBA& color, int grid, const char* text);
	void draw_text(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color, int x, int y, const char* text);
	void draw_graph(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height);

private:
	std::map<string, Graph*> _graph;

	double _x_max_range = 0.0;
	double _y_max_range = 0.0;
};

#endif /* GTKGRAPH_HPP_ */
