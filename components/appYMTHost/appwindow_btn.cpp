/*
 * appwindow_btn.cpp
 *
 *  Created on: 2015. 5. 19.
 *      Author: hwang-linux
 */


#include "appwindow.hpp"
#include <string>
#include <vector>
#include "format.h"
#include <gdkmm-3.0/gdkmm/rectangle.h>
#include "../../include/core/component_broker.hpp"

using namespace std;

void appwindow::btn_init()
{
	/*_builder->get_widget("ID_BTN_SAMPLE", _btn_sample);
	_builder->get_widget("ID_BTN_PUMP_A", _btn_apump);
	_builder->get_widget("ID_BTN_PUMP_B", _btn_bpump);
	_builder->get_widget("ID_BTN_PUMP_C", _btn_cpump);
	_builder->get_widget("ID_BTN_PUMP_D", _btn_dpump);
	_builder->get_widget("ID_BTN_PUMP_E", _btn_epump);*/
	_builder->get_widget("ID_BTN_SETTING", _btn_setting);

	_builder->get_widget("ID_SAMPLE_TEXT", _sample_text);
	_builder->get_widget("ID_APUMP_TEXT", _apump_text);
	_builder->get_widget("ID_BPUMP_TEXT", _bpump_text);
	_builder->get_widget("ID_CPUMP_TEXT", _cpump_text);
	_builder->get_widget("ID_DPUMP_TEXT", _dpump_text);
	_builder->get_widget("ID_EPUMP_TEXT", _epump_text);

	_builder->get_widget("ID_SAMPLE_ICON", _sample_icon);
	_builder->get_widget("ID_APUMP_ICON", _apump_icon);
	_builder->get_widget("ID_BPUMP_ICON", _bpump_icon);
	_builder->get_widget("ID_CPUMP_ICON", _cpump_icon);
	_builder->get_widget("ID_DPUMP_ICON", _dpump_icon);
	_builder->get_widget("ID_EPUMP_ICON", _epump_icon);


	/*
	 * change password dialog
	 */
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_ESC", _btn_change_pw_key_esc);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_OK", _btn_change_pw_key_ok);
	_builder->get_widget("ID_PASSWORD_VALUE_DEL", _btn_change_pw_key_del);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_0", _btn_change_pw_key[0]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_1", _btn_change_pw_key[1]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_2", _btn_change_pw_key[2]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_3", _btn_change_pw_key[3]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_4", _btn_change_pw_key[4]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_5", _btn_change_pw_key[5]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_6", _btn_change_pw_key[6]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_7", _btn_change_pw_key[7]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_8", _btn_change_pw_key[8]);
	_builder->get_widget("ID_DIALOG_PASSWORD_NUMBER_9", _btn_change_pw_key[9]);
	_builder->get_widget("ID_PASSWORD_NEW", _edit_new_password);
	_builder->get_widget("ID_PASSWORD_NEW_CONFIRM", _edit_password_confirm);


	/*
	 * password for entering setting menu
	 */
	_builder->get_widget("ID_BTN_PASSWORD_0", _btn_password_numberpad[0]);
	_builder->get_widget("ID_BTN_PASSWORD_1", _btn_password_numberpad[1]);
	_builder->get_widget("ID_BTN_PASSWORD_2", _btn_password_numberpad[2]);
	_builder->get_widget("ID_BTN_PASSWORD_3", _btn_password_numberpad[3]);
	_builder->get_widget("ID_BTN_PASSWORD_4", _btn_password_numberpad[4]);
	_builder->get_widget("ID_BTN_PASSWORD_5", _btn_password_numberpad[5]);
	_builder->get_widget("ID_BTN_PASSWORD_6", _btn_password_numberpad[6]);
	_builder->get_widget("ID_BTN_PASSWORD_7", _btn_password_numberpad[7]);
	_builder->get_widget("ID_BTN_PASSWORD_8", _btn_password_numberpad[8]);
	_builder->get_widget("ID_BTN_PASSWORD_9", _btn_password_numberpad[9]);
	_builder->get_widget("ID_BTN_PASSWORD_BACK", _btn_password_back);
	_builder->get_widget("ID_BTN_PASSWORD_OK", _btn_password_ok);
	_builder->get_widget("ID_BTN_PASSWORD_ESC", _btn_password_esc);



	_builder->get_widget("ID_BTN_SETTING_PROGRAM_EXIT", _btn_setting_menu[0]);
	_builder->get_widget("ID_BTN_SETTING_PLATING", _btn_setting_menu[1]);
	_builder->get_widget("ID_BTN_SETTING_OTHERS", _btn_setting_menu[2]);
	_builder->get_widget("ID_BTN_SETTING_PUMP", _btn_setting_menu[3]);
	_builder->get_widget("ID_BTN_SETTING_MTO_RESET", _btn_setting_menu[4]);
	_builder->get_widget("ID_BTN_SETTING_BACK", _btn_setting_menu[5]);

	_builder->get_widget("ID_BTN_SPUMP_SPEED_UP", _btn_spump_speed_up);
	_builder->get_widget("ID_BTN_SPUMP_SPEED_DOWN", _btn_spump_speed_down);

	_builder->get_widget("ID_BTN_CHANGE_PASSWORD", _btn_change_password);


	/*if(_btn_sample) _btn_sample->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_sample_click));
	if(_btn_apump) _btn_apump->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_apump_click));
	if(_btn_bpump) _btn_bpump->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_bpump_click));
	if(_btn_cpump) _btn_cpump->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_cpump_click));
	if(_btn_dpump) _btn_dpump->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_dpump_click));
	if(_btn_epump) _btn_epump->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_epump_click));*/
	if(_btn_setting) _btn_setting->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_setting_click));
	if(_btn_password_numberpad[0]) _btn_password_numberpad[0]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_0_click));
	if(_btn_password_numberpad[1]) _btn_password_numberpad[1]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_1_click));
	if(_btn_password_numberpad[2]) _btn_password_numberpad[2]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_2_click));
	if(_btn_password_numberpad[3]) _btn_password_numberpad[3]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_3_click));
	if(_btn_password_numberpad[4]) _btn_password_numberpad[4]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_4_click));
	if(_btn_password_numberpad[5]) _btn_password_numberpad[5]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_5_click));
	if(_btn_password_numberpad[6]) _btn_password_numberpad[6]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_6_click));
	if(_btn_password_numberpad[7]) _btn_password_numberpad[7]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_7_click));
	if(_btn_password_numberpad[8]) _btn_password_numberpad[8]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_8_click));
	if(_btn_password_numberpad[9]) _btn_password_numberpad[9]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_9_click));
	if(_btn_password_back) _btn_password_back->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_back_click));
	if(_btn_password_ok) _btn_password_ok->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_ok_click));
	if(_btn_password_esc) _btn_password_esc->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_password_esc_click));

	if(_btn_setting_menu[0]) _btn_setting_menu[0]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_setting_exit_click));
	if(_btn_setting_menu[1]) _btn_setting_menu[1]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_setting_plating_click));
	if(_btn_setting_menu[2]) _btn_setting_menu[2]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_setting_others_click));
	if(_btn_setting_menu[3]) _btn_setting_menu[3]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_setting_pump_click));
	if(_btn_setting_menu[4]) _btn_setting_menu[4]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_setting_mto_click));
	if(_btn_setting_menu[5]) _btn_setting_menu[5]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_setting_back_click));

	if(_btn_spump_speed_up) _btn_spump_speed_up->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_spump_speedup_click));
	if(_btn_spump_speed_down) _btn_spump_speed_down->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_spump_speeddown_click));


	if(_btn_change_password) _btn_change_password->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_click));

	/*
	 * change password dialog event
	 */
	if(_btn_change_pw_key_esc) _btn_change_pw_key_esc->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_esc_click));
	if(_btn_change_pw_key_ok) _btn_change_pw_key_ok->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_ok_click));
	if(_btn_change_pw_key_del) _btn_change_pw_key_del->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_del_click));
	if(_btn_password_numberpad[0]) _btn_password_numberpad[0]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_0_click));
	if(_btn_password_numberpad[1]) _btn_password_numberpad[1]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_1_click));
	if(_btn_password_numberpad[2]) _btn_password_numberpad[2]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_2_click));
	if(_btn_password_numberpad[3]) _btn_password_numberpad[3]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_3_click));
	if(_btn_password_numberpad[4]) _btn_password_numberpad[4]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_4_click));
	if(_btn_password_numberpad[5]) _btn_password_numberpad[5]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_5_click));
	if(_btn_password_numberpad[6]) _btn_password_numberpad[6]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_6_click));
	if(_btn_password_numberpad[7]) _btn_password_numberpad[7]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_7_click));
	if(_btn_password_numberpad[8]) _btn_password_numberpad[8]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_8_click));
	if(_btn_password_numberpad[9]) _btn_password_numberpad[9]->signal_clicked().connect(sigc::mem_fun(*this, &appwindow::on_btn_change_password_9_click));
}

void appwindow::on_btn_setting_exit_click()
{
	int result = _exit_dialog->run();
	switch(result)
	{
	case Gtk::RESPONSE_YES:
		exit(1);
		break;
	case Gtk::RESPONSE_NO:
		break;
	}
	_exit_dialog->close();
}

void appwindow::on_btn_setting_plating_click()
{
	_setting_tab->set_current_page(SETTING_INDEX::PLATING);
}

void appwindow::on_btn_setting_others_click()
{
	_setting_tab->set_current_page(SETTING_INDEX::OTHERS);
}

void appwindow::on_btn_setting_pump_click()
{
	_setting_tab->set_current_page(SETTING_INDEX::PUMP);
}

void appwindow::on_btn_setting_mto_click()
{

}

/*
 * return to mani window
 */
void appwindow::on_btn_setting_back_click() {

	if(_setting_tab->get_current_page()!=SETTING_INDEX::MAIN)
		_setting_tab->set_current_page(SETTING_INDEX::MAIN);
	else
	{

	_edit_password->set_text("");

	int result = _save_dialog->run();
	switch(result)
	{
	case Gtk::RESPONSE_YES:
		for(auto prop:_property) {

		}

		break;
	case Gtk::RESPONSE_NO:
		break;
	}

	_save_dialog->close();
	_right_tab->set_current_page(FRAME_INDEX::MAIN_FRAME);
	}
}


void appwindow::on_btn_sample_click()
{
	/*if(_property.find("pub")!=_property.end() && _property.find("name")!=_property.end()) {
		vector<char> data;
		plunframework::broker::component_broker::get()->publish(_property["name"].c_str(),_property["pub"].c_str(), "raw", data);
	}*/
}

void appwindow::on_btn_apump_click()
{

}

void appwindow::on_btn_bpump_click()
{
	//_error_dialog->hide();
}

void appwindow::on_btn_cpump_click()
{

}

void appwindow::on_btn_dpump_click()
{

}

void appwindow::on_btn_epump_click()
{

}


void appwindow::in_password(const char* pw)
{
	string cur = _edit_password->get_text();
	_edit_password->set_text(cur+pw);
}

void appwindow::on_btn_setting_click()
{
	_right_tab->set_current_page(FRAME_INDEX::PASSWORD_FRAME);
}

void appwindow::on_btn_password_0_click(){ in_password("0"); }
void appwindow::on_btn_password_1_click(){ in_password("1"); }
void appwindow::on_btn_password_2_click(){ in_password("2"); }
void appwindow::on_btn_password_3_click(){ in_password("3"); }
void appwindow::on_btn_password_4_click(){ in_password("4"); }
void appwindow::on_btn_password_5_click(){ in_password("5"); }
void appwindow::on_btn_password_6_click(){ in_password("6"); }
void appwindow::on_btn_password_7_click(){ in_password("7"); }
void appwindow::on_btn_password_8_click(){ in_password("8"); }
void appwindow::on_btn_password_9_click(){ in_password("9"); }

void appwindow::on_btn_password_back_click()
{
	string cur = _edit_password->get_text();
	if(cur.size()>0)
		cur = cur.substr(0, cur.size()-1);
	_edit_password->set_text(cur);
}

void appwindow::on_btn_password_ok_click()
{
	string cur = _edit_password->get_text();
	if(_property.find("password")!=_property.end())
		if(!cur.compare(_property["password"]))
			_right_tab->set_current_page(FRAME_INDEX::SETTING_FRAME);
}

void appwindow::on_btn_password_esc_click()
{
	_edit_password->set_text("");
	_right_tab->set_current_page(FRAME_INDEX::MAIN_FRAME);
}

void appwindow::on_btn_spump_speeddown_click()
{
	double level = _spump_levelbar->get_value();
	level -= 1.0;
	if(level<0.0)
		level=0.0;
	_spump_levelbar->set_value(level);
	_spump_speed_label->set_text(fmt::format("{}",(int)level));
}

void appwindow::on_btn_spump_speedup_click()
{
	double level = _spump_levelbar->get_value();
	level += 1.0;
	if(level>10.0)
		level=10.0;
	_spump_levelbar->set_value(level);
	_spump_speed_label->set_text(fmt::format("{}",(int)level));
}

void appwindow::on_btn_change_password_click()
{
	_change_pw_dialog->show();
}

void appwindow::on_btn_change_password_esc_click()
{
	_change_pw_dialog->hide();
}

void appwindow::on_btn_change_password_ok_click()
{

}

void appwindow::on_btn_change_password_del_click()
{

}

void appwindow::on_btn_change_password_0_click() { }
void appwindow::on_btn_change_password_1_click() { }
void appwindow::on_btn_change_password_2_click() { }
void appwindow::on_btn_change_password_3_click() { }
void appwindow::on_btn_change_password_4_click() { }
void appwindow::on_btn_change_password_5_click() { }
void appwindow::on_btn_change_password_6_click() { }
void appwindow::on_btn_change_password_7_click() { }
void appwindow::on_btn_change_password_8_click() { }
void appwindow::on_btn_change_password_9_click() { }
