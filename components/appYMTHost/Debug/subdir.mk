################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../format.cc 

CPP_SRCS += \
../appYMTHost.cpp \
../appwindow.cpp \
../appwindow_btn.cpp \
../gtkGraph.cpp 

CC_DEPS += \
./format.d 

OBJS += \
./appYMTHost.o \
./appwindow.o \
./appwindow_btn.o \
./format.o \
./gtkGraph.o 

CPP_DEPS += \
./appYMTHost.d \
./appwindow.d \
./appwindow_btn.d \
./gtkGraph.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -I/usr/include -I/usr/local/include -O0 -g3 -Wall -c -fmessage-length=0 $(shell pkg-config --cflags glibmm-2.4 giomm-2.4 gtkmm-3.0) -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -I/usr/include -I/usr/local/include -O0 -g3 -Wall -c -fmessage-length=0 $(shell pkg-config --cflags glibmm-2.4 giomm-2.4 gtkmm-3.0) -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


