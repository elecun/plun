/*
 * gtkGraph.cpp
 *
 *  Created on: 2015. 5. 18.
 *      Author: hwang-linux
 */

#include "gtkGraph.hpp"
#include <cairomm-1.0/cairomm/enums.h>
#include <gtkmm-3.0/gtkmm/widget.h>
#include <iostream>
#include "format.h"

const double left_offset = 25;
const double right_offset = 10;
const double top_offset = 10;
const double bottom_offset = 15;

gtkGraph::gtkGraph() {


}

gtkGraph::~gtkGraph() {
	for(auto& itr:_graph) {
		delete itr.second;
	}
}

void gtkGraph::on_draw(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation)
{
	cr->set_antialias(Antialias::ANTIALIAS_NONE);

	GdkRGBA white = {1.0, 1.0, 1.0, 1.0};
	GdkRGBA black = {0.0, 0.0, 0.0, 1.0};
	GdkRGBA grey = {0.7, 0.7, 0.7, 1.0};

	double screen_width = allocation->get_width();
	double screen_height = allocation->get_height();

	draw_background(cr, screen_width, screen_height, white);
	draw_window_outline(cr, screen_width, screen_height, black, 1.0);
	draw_graph_outline(cr, screen_width, screen_height, black, 1.0);
	draw_grid(cr, screen_width, screen_height, grey, 1.0, 40);
	draw_graph(cr, screen_width, screen_height);
	draw_xlabel(cr, allocation, screen_width, screen_height, black, 90, "Time");
	draw_ylabel(cr, allocation, screen_width, screen_height, black, 10, "Amplitude");

	/*draw_background(cr, allocation, white);
	draw_window_outline(cr, allocation, black, 1.0);
	draw_graph_outline(cr, allocation, black, 1.0);
	draw_grid(cr, allocation, grey, 1.0, 40);
	draw_graph(cr, allocation);
	draw_xlabel(cr, allocation, black, 100, "Time");
	draw_ylabel(cr, allocation, black, 40, "Amplitude");*/
}

void gtkGraph::add_graph(const char* graph_title, GdkRGBA& color)
{
	if(_graph.find(graph_title)==_graph.end())
		_graph.insert(pair<string, Graph*>(graph_title, new Graph(graph_title, color, _x_max_range)));
}

void gtkGraph::push_data(const char* graph_title, double data)
{
	if(_graph.find(graph_title)!=_graph.end())
		_graph[graph_title]->push(data);
}

void gtkGraph::draw_window_outline(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color, double width)
{
	cr->set_line_width(width);
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->rectangle(1, 1, screen_width-1, screen_height-1);
	cr->stroke();
}

void gtkGraph::draw_graph_outline(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color, double width)
{
	const int w = screen_width;
	const int h = screen_height;

	cr->set_line_width(width);
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->rectangle(left_offset, top_offset, w-left_offset-right_offset, h-top_offset-bottom_offset);
	cr->stroke();
}

void gtkGraph::draw_background(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color)
{
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->rectangle(1, 1, screen_width-1, screen_height-1);
	cr->fill();
}

void gtkGraph::draw_grid(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color, double width, int grid)
{
	const int w = screen_width;
	const int h = screen_height;

	cr->set_source_rgb(color.red, color.green, color.blue);

	vector<double> pattern;
	pattern.push_back(1);
	pattern.push_back(3);
	cr->set_dash(pattern, 1);

	//horizontal
	for(int i=h-bottom_offset-grid;i>top_offset;i-=grid)
	{
		cr->move_to(left_offset, i);
		cr->line_to(w-right_offset-1, i);
	}

	cr->stroke();
	cr->unset_dash();
}

void gtkGraph::draw_xlabel(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, double screen_width, double screen_height, GdkRGBA& color, int grid, const char* text)
{
	GdkRGBA black = {0.0, 0.0, 0.0, 1.0};
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->set_line_width(1);
	for(int i=left_offset+grid;i<screen_width;i+=grid)
	{
		cr->move_to(i, screen_height-bottom_offset);
		cr->line_to(i, screen_height-bottom_offset+3);
	}
	cr->stroke();

	draw_text(cr, allocation, black, (screen_width-40)/2, screen_height-5, fmt::format("10 min").c_str());
	draw_text(cr, allocation, color, screen_width-40, screen_height-5, text);
}

void gtkGraph::draw_ylabel(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, double screen_width, double screen_height, GdkRGBA& color, int grid, const char* text)
{
	const int h = screen_height;
	GdkRGBA black = {0.0, 0.0, 0.0, 1.0};

	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->set_line_width(1);
	for(int i=h-bottom_offset-grid;i>top_offset;i-=grid)
	{
		cr->move_to(left_offset, i);
		cr->line_to(left_offset-3, i);
	}
	cr->stroke();

	//cr->translate(15, screen_height-bottom_offset);
	draw_text(cr, allocation, black, 15, screen_height-bottom_offset, fmt::format("0").c_str());
	//cr->translate(0, -1*(screen_height-bottom_offset)/2);
	draw_text(cr, allocation, black, 15, (screen_height-bottom_offset)/2, fmt::format("{}",_y_max_range/2).c_str());
	cr->translate(15, 55);
	cr->rotate_degrees(-90);
	draw_text(cr, allocation, color, 0,0, text);
}

void gtkGraph::draw_text(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height, GdkRGBA& color, int x, int y, const char* text)
{
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->move_to(x,y);
	Cairo::RefPtr<Cairo::ToyFontFace> font = Cairo::ToyFontFace::create("Arial",Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_BOLD);
	cr->set_font_face(font);
	cr->set_font_size(10);
	cr->show_text(text);
}

void gtkGraph::draw_graph(const Cairo::RefPtr<Cairo::Context>& cr, double screen_width, double screen_height)
{
	const double w_unit = (screen_width-left_offset-right_offset)/_x_max_range;
	const double h_unit = (screen_height-top_offset-bottom_offset)/_y_max_range;

	//cr->move_to(left_offset, screen_height-bottom_offset);	//move to origin
	for(auto& itr:_graph) {
		GdkRGBA* color = itr.second->get_color();
		cr->set_source_rgb(color->red, color->green, color->blue);
		cr->set_line_width(2);
		double xpos = left_offset;
		double ypos = screen_height-bottom_offset-itr.second->get(0)*h_unit;

		cr->move_to(xpos, ypos);
		for(unsigned int i=0;i<itr.second->get_data_size();i++) {
			xpos = left_offset+i*w_unit;
			ypos = screen_height-bottom_offset-itr.second->get(i)*h_unit;
			cr->line_to(xpos, ypos);
		}

		cr->stroke();
	}
}


void gtkGraph::draw_window_outline(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, double width)
{
	cr->set_line_width(width);
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->rectangle(1, 1, allocation->get_width()-1, allocation->get_height()-1);
	cr->stroke();
}

void gtkGraph::draw_graph_outline(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, double width)
{
	const int w = allocation->get_width();
	const int h = allocation->get_height();

	cr->set_line_width(width);
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->rectangle(left_offset, top_offset, w-left_offset-right_offset, h-top_offset-bottom_offset);
	cr->stroke();
}

void gtkGraph::draw_background(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color)
{
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->rectangle(1, 1, allocation->get_width()-1, allocation->get_height()-1);
	cr->fill();
}

void gtkGraph::draw_grid(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, double width, int grid)
{
	const int w = allocation->get_width();
	const int h = allocation->get_height();

	cr->set_source_rgb(color.red, color.green, color.blue);

	vector<double> pattern;
	pattern.push_back(1);
	pattern.push_back(3);
	cr->set_dash(pattern, 1);

	//vertical
	/*for(int i=left_offset+grid;i<w;i+=grid)
	{
		cr->move_to(i, top_offset);
		cr->line_to(i, h-bottom_offset-1);
	}*/

	//horizontal
	for(int i=h-bottom_offset-grid;i>top_offset;i-=grid)
	{
		cr->move_to(left_offset, i);
		cr->line_to(w-right_offset-1, i);
	}

	cr->stroke();
	cr->unset_dash();
}

void gtkGraph::draw_xlabel(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, int grid, const char* text)
{
	const int w = allocation->get_width();
	const int h = allocation->get_height();

	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->set_line_width(1);
	for(int i=left_offset+grid;i<w;i+=grid)
	{
		cr->move_to(i, h-bottom_offset);
		cr->line_to(i, h-bottom_offset+3);
	}
	cr->stroke();
	draw_text(cr, allocation, color, w-40, h-5, text);
}

void gtkGraph::draw_ylabel(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, int grid, const char* text)
{
	const int h = allocation->get_height();

	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->set_line_width(1);
	for(int i=h-bottom_offset-grid;i>top_offset;i-=grid)
	{
		cr->move_to(left_offset, i);
		cr->line_to(left_offset-3, i);
	}
	cr->stroke();
	cr->translate(15, 55);
	cr->rotate_degrees(-90);
	draw_text(cr, allocation, color, 0, 0, text);
}

void gtkGraph::draw_text(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation, GdkRGBA& color, int x, int y, const char* text)
{
	cr->set_source_rgb(color.red, color.green, color.blue);
	cr->move_to(x,y);
	Cairo::RefPtr<Cairo::ToyFontFace> font = Cairo::ToyFontFace::create("Arial",Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_BOLD);
	cr->set_font_face(font);
	cr->set_font_size(10);
	cr->show_text(text);
}

void gtkGraph::draw_graph(const Cairo::RefPtr<Cairo::Context>& cr, Gtk::Allocation* allocation)
{
	const double w_unit = (allocation->get_width()-left_offset-right_offset)/_x_max_range;
	const double h_unit = (allocation->get_height()-top_offset-bottom_offset)/_y_max_range;

	//cr->move_to(left_offset, allocation->get_height()-bottom_offset);	//move to origin
	for(auto& itr:_graph) {
		GdkRGBA* color = itr.second->get_color();
		cr->set_source_rgb(color->red, color->green, color->blue);
		cr->set_line_width(2);
		double xpos = left_offset;
		double ypos = allocation->get_height()-bottom_offset-itr.second->get(0)*h_unit;

		cr->move_to(xpos, ypos);
		for(unsigned int i=0;i<itr.second->get_data_size();i++) {
			xpos = left_offset+i*w_unit;
			ypos = allocation->get_height()-bottom_offset-itr.second->get(i)*h_unit;
			cr->line_to(xpos, ypos);
		}

		cr->stroke();
	}
}

