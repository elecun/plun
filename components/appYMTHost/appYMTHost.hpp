/*
 * appYMTHost.hpp
 *
 *  Created on: 2015. 5. 16.
 *      Author: hwang-linux
 */

#ifndef APPYMTHOST_HPP_
#define APPYMTHOST_HPP_

#include "../../include/core/interface/icomponent_gui.hpp"
#include <gtkmm-3.0/gtkmm.h>
#include <cairomm-1.0/cairomm/cairomm.h>
#include <glibmm-2.4/glibmm.h>
#include <string>
#include "protocol.hpp"
#include <boost/thread/mutex.hpp>

using namespace std;

class appwindow;
namespace plunframework {
namespace component {

class appYMTHost : public interface::icomponent_gui {
public:
	appYMTHost();
	virtual ~appYMTHost();

	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

	bool loop();

private:
	void run_task();
	void print_hex(unsigned char* data, int len);

private:
	appwindow* _gui_window = nullptr;		//graphic user interface window

	process::task _run_task;				//run with gui window
	string _topic;					// topic from profile

	Glib::RefPtr<Gtk::Application> _app;	//gui application
	Glib::RefPtr<Gtk::Builder> _builder;	//builder for glade

	PACKET_REQUEST _request_data;
	boost::mutex _mutex;
	unsigned int _rcv_check = 0;
};

COMPONENT_EXPORT

} /* namespace component */
} /* namespace plunframework */

#endif /* APPYMTHOST_HPP_ */
