/*
 * appwindow.cpp
 *
 *  Created on: 2015. 5. 16.
 *      Author: hwang-linux
 */

#include "appwindow.hpp"
#include <glibmm-2.4/glibmm/fileutils.h>	//for FileError
#include <glibmm-2.4/glibmm/markup.h>		//for markup
#include <gtkmm-3.0/gtkmm/widget.h>
#include <gdkmm-3.0/gdkmm/rgba.h>
#include <pangomm-1.4/pangomm/attrlist.h>
#include "format.h"
#include <ctime>
#include "../../include/core/component_broker.hpp"


appwindow::appwindow(BaseObjectType* obj, const Glib::RefPtr<Gtk::Builder>& glade)
:Gtk::Window(obj),_builder(glade)
{
	this->Window::fullscreen();
	//GdkRGBA grey = {0.45, 0.45, 0.45, 1.0};
	//this->override_background_color(Gdk::RGBA(&grey));

	/*
	 * initialize
	 */
	btn_init();

	/*
	 * window & dialog widget
	 */
	_builder->get_widget("ID_DIALOG_EXIT_PROGRAM", _exit_dialog);
	_builder->get_widget("ID_DIALOG_EXIT_PROGRAM", _keypad_dialog);
	_builder->get_widget("ID_DIALOG_SAVE", _save_dialog);
	_builder->get_widget("ID_DIALOG_PASSWORD", _change_pw_dialog);


	/*
	 * error led indicator
	 */
	_builder->get_widget("ID_LED_NI_MAX_ERROR",_ni_max_error);	_ni_max_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_NI_MIN_ERROR",_ni_min_error);	_ni_min_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_PH_MAX_ERROR",_ph_max_error);	_ph_max_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_PH_MIN_ERROR",_ph_min_error);	_ph_min_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_TEMP_MAX_ERROR",_temp_max_error);	_temp_max_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_TEMP_MIN_ERROR",_temp_min_error);	_temp_min_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_PC_COM_ERROR",_pc_com_error);			_pc_com_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_PC_LEN_ERROR",_pc_len_error);			_pc_len_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_PC_BCC_ERROR",_pc_bcc_error);			_pc_bcc_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_CUWIN_COM_ERROR",_cuwin_com_error);	_cuwin_com_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_CUWIN_LEN_ERROR",_cuwin_len_error);	_cuwin_len_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_CUWIN_BCC_ERROR",_cuwin_bcc_error);	_cuwin_bcc_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_NI_COM_ERROR",_ni_com_error);			_ni_com_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_MOTOR_COM_ERROR",_motor_com_error);		_motor_com_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_BUZZER_STOP",_buzzer_stop_error);		_buzzer_stop_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_SPUMP_SPEED_ERROR",_spump_speed_error);	_spump_speed_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_SPUMP_ERROR",_spump_error);	_spump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_APUMP_ERROR",_apump_error);	_apump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_BPUMP_ERROR",_bpump_error);	_bpump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_CPUMP_ERROR",_cpump_error);	_cpump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_DPUMP_ERROR",_dpump_error);	_dpump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	_builder->get_widget("ID_LED_EPUMP_ERROR",_epump_error);	_epump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);


	/*
	 * setting main menu
	 */

	_builder->get_widget("ID_SETTING_MAIN_FRAME", _setting_tab);
	_setting_tab->set_current_page(SETTING_INDEX::MAIN);

	_builder->get_widget("ID_GRAPH_TAB", _graph_tab);
	_graph_tab->set_current_page(GRAPH_INDEX::GRAPH);


	_builder->get_widget("ID_EDIT_PASSWORD", _edit_password);


	_builder->get_widget("ID_MODE_AUTO", _sw_auto_mode);
	//_sw_auto_mode->signal_state_changed().connect(sigc::mem_fun(*this, &appwindow::on_sw_mode_changed));

	_builder->get_widget("ID_GRAPH_DRAWAREA", _graph_window);
	_builder->get_widget("ID_DATA_MESSAGE_LOG", _message_window);
	_builder->get_widget("ID_ALARM_MESSAGE_LOG", _alarm_window);
	_builder->get_widget("ID_RIGHT_FRAME", _right_tab);

	_builder->get_widget("ID_SPUMP_SPEED_BAR", _spump_levelbar);
	_builder->get_widget("ID_SPUMP_SPEED_VALUE", _spump_speed_label);


	//label
	_builder->get_widget("ID_MTO_VALUE", _monitor_mto);
	_builder->get_widget("ID_PH_VALUE", _monitor_ph);
	_builder->get_widget("ID_NI_VALUE", _monitor_ni);
	_builder->get_widget("ID_TEMP_VALUE", _monitor_temp);
	_builder->get_widget("ID_NI_SENSOR_VALUE", _monitor_ni_sensor);
	_builder->get_widget("ID_CURRENT_NI_VALUE", _monitor_current_ni);
	_builder->get_widget("ID_CURRENT_PH_VALUE", _monitor_current_ph);
	_builder->get_widget("ID_CURRENT_TEMP_VALUE", _monitor_current_temp);


	if(_spump_levelbar)
		_spump_levelbar->set_value(0.0);

	_right_tab->set_current_page(FRAME_INDEX::MAIN_FRAME);

	if(_graph_window) {
		_graph_impl = new gtkGraph();
		_graph_impl->set_data_range(1200, 10);	//20min
		_graph_window->signal_draw().connect(sigc::mem_fun(*this, &appwindow::on_draw_graph));
		GdkRGBA red = {1.0, 0.0, 0.0, 1.0};
		GdkRGBA blue = {0.0, 0.0, 1.0, 1.0};

		_graph_impl->add_graph("Ni", red);
		_graph_impl->add_graph("pH", blue);

	}

	_monitor_ni->set_use_markup(true);
	_monitor_ph->set_use_markup(true);
	_monitor_temp->set_use_markup(true);
	_monitor_mto->set_use_markup(true);

	_monitor_mto->set_markup("<span foreground=\"orange\">0.00</span>");
	_monitor_ni->set_markup("<span foreground=\"orange\">0.00</span>");
	_monitor_ph->set_markup("<span foreground=\"orange\">0.00</span>");
	_monitor_temp->set_markup("<span foreground=\"orange\">0.00</span>");

}


appwindow::~appwindow() {

	if(_alarm_logfile->is_open())
		_alarm_logfile->close();

	if(_graph_impl)
		delete _graph_impl;

	hide();
}

void appwindow::init()
{
	_alarm_logfile = new std::ofstream;

	//extract path
	string path = _property["alaram_log_prefix"];

	//create path
	boost::filesystem::path dir(path);
	if(!(boost::filesystem::exists(dir)))
		boost::filesystem::create_directory(dir);
}

void appwindow::log_alarm(const char* text)
{
	time_t now = time(0);
	tm *ltm = localtime(&now);

	string prefix = _property["alaram_log_prefix"];
	string f = fmt::format("{}{}-{}-{} {}-{}-{}.txt",prefix,(1900+ltm->tm_year), (1+ltm->tm_mon), (ltm->tm_mday), (ltm->tm_hour), 0,0);

	if(_filename.empty() || _filename.compare(f)!=0)
	{
		_filename = f;
		_alarm_logfile->close();
		_alarm_logfile->open(_filename, ios_base::out|ios_base::app);
	}

	string logstr = fmt::format("[{}-{}-{} {}-{}-{}]\t{}\n",(1900+ltm->tm_year), (1+ltm->tm_mon), (ltm->tm_mday), (ltm->tm_hour), (ltm->tm_min), (ltm->tm_sec), text);

	if(_alarm_logfile->is_open()) {
		*_alarm_logfile << logstr;
	}

	Glib::RefPtr<Gtk::TextBuffer> _alarm_buffer = _alarm_window->get_buffer();

	if(_alarm_buffer->get_line_count()>10)
		_alarm_buffer->set_text("");

	_alarm_buffer->insert(_alarm_buffer->begin(), logstr);
}

void appwindow::log_message(const char* text)
{
	Glib::RefPtr<Gtk::TextBuffer> _message_buffer = _message_window->get_buffer();

	_message_buffer->insert(_message_buffer->begin(), text);

	if(_message_buffer->get_line_count()>10) {
		_message_buffer->set_text("");
	}
}


bool appwindow::on_draw_graph(const Cairo::RefPtr<Cairo::Context>& cr)
{
	if(_graph_impl)
		_graph_impl->on_draw(cr, (Gtk::Allocation*)_graph_window->get_allocation().gobj());

	return true;
}

void appwindow::set_property(const char* name, const char* value)
{
	_property[name] = value;
}

void appwindow::refresh()
{
	this->queue_draw();
}

void appwindow::pump_process(unsigned char pump_code)
{

	if(pump_code&APUMP) {
		_apump_text->set_text("A Pump ON");
		_apump_icon->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}
	else {
		_apump_text->set_text("A Pump OFF");
		_apump_icon->set_from_icon_name("window-close", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}

	if(pump_code&BPUMP) {
		_bpump_text->set_text("B Pump ON");
		_bpump_icon->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}
	else {
		_bpump_text->set_text("B Pump OFF");
		_bpump_icon->set_from_icon_name("window-close", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}

	if(pump_code&CPUMP) {
		_cpump_text->set_text("C Pump ON");
		_cpump_icon->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}
	else {
		_cpump_text->set_text("C Pump OFF");
		_cpump_icon->set_from_icon_name("window-close", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}

	if(pump_code&DPUMP) {
		_dpump_text->set_text("D Pump ON");
		_dpump_icon->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}
	else {
		_dpump_text->set_text("D Pump OFF");
		_dpump_icon->set_from_icon_name("window-close", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}

	if(pump_code&EPUMP) {
		_epump_text->set_text("E Pump ON");
		_epump_icon->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}
	else {
		_epump_text->set_text("E Pump OFF");
		_epump_icon->set_from_icon_name("window-close", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}

	if(pump_code&SPUMP) {
		_sample_text->set_text("Sample ON");
		_sample_icon->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}
	else {
		_sample_text->set_text("Sample OFF");
		_sample_icon->set_from_icon_name("window-close", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}

}

void appwindow::mode_process(unsigned char mode_code)
{
	if(mode_code&MODE_MANUAL)
		_sw_auto_mode->set_active(false);
	else
		_sw_auto_mode->set_active(true);
}

void appwindow::state_process(unsigned char state_code)
{

	if(state_code&EMERGENCY_MODE_SW) { _graph_tab->set_current_page(GRAPH_INDEX::EMERGENCY); }

	if(state_code&PWR_MODE) {
		_graph_tab->set_current_page(GRAPH_INDEX::PWRDOWN);
	}

	if(state_code&BUZZER_STOP_SW) {
		_buzzer_stop_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
		_graph_tab->set_current_page(GRAPH_INDEX::ERROR);
	}
	else {
		_buzzer_stop_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
	}
}

void appwindow::error_process(PACKET_RESPONSE* data, unsigned int error_code)
{

	bool error = false;

	if(data!=nullptr)
	{
		if(error_code&ERROR_NI_MAX) {
			_ni_max_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true;
			_monitor_ni->set_markup(fmt::format("<span foreground=\"#ff0000\">{}</span>",data->get_ni_gram()));
		}
		else
			_ni_max_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);

		if(error_code&ERROR_NI_MIN) {
			_ni_min_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true;
			_monitor_ni->set_markup(fmt::format("<span foreground=\"#0000ff\">{}</span>",data->get_ni_gram()));
		}
		else
			_ni_min_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);


		//if not error
		if(!(error_code&ERROR_NI_MAX) && !(error_code&ERROR_NI_MIN))
			_monitor_ni->set_markup(fmt::format("<span foreground=\"orange\">{}</span>",data->get_ni_gram()));


		if(error_code&ERROR_PH_MAX) {
			_ph_max_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true;
			_monitor_ph->set_markup(fmt::format("<span foreground=\"#ff0000\">{}</span>",data->get_ph()));
		}
		else {
			_ph_max_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
		}

		if(error_code&ERROR_PH_MIN) {
			_ph_min_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true;
			_monitor_ph->set_markup(fmt::format("<span foreground=\"#0000ff\">{}</span>",data->get_ph()));
		}
		else {
			_ph_min_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
		}

		//if not error
		if(!(error_code&ERROR_PH_MAX) && !(error_code&ERROR_PH_MIN))
			_monitor_ph->set_markup(fmt::format("<span foreground=\"orange\">{}</span>",data->get_ph()));

		if(error_code&ERROR_TEMP_MAX) {
			_temp_max_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true;
			_monitor_temp->set_markup(fmt::format("<span foreground=\"#ff0000\">{}</span>",data->get_temperature()));
		}
		else {
			_temp_max_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
		}

		if(error_code&ERROR_TEMP_MIN) {
			_temp_min_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true;
			_monitor_temp->set_markup(fmt::format("<span foreground=\"#0000ff\">{}</span>",data->get_temperature()));
		}
		else {
			_temp_min_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
		}

		//if not error
		if(!(error_code&ERROR_TEMP_MAX) && !(error_code&ERROR_TEMP_MIN))
			_monitor_temp->set_markup(fmt::format("<span foreground=\"orange\">{}</span>",data->get_temperature()));
	}


	if(error_code&ERROR_MB_COM) { _pc_com_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _pc_com_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_MB_LEN) { _pc_len_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _pc_len_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_MB_BCC) { _pc_bcc_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _pc_bcc_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_CUWIN_COM) { _cuwin_com_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _cuwin_com_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_CUWIN_LEN) { _cuwin_len_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _cuwin_len_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_CUWIN_BCC) { _cuwin_bcc_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _cuwin_bcc_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_UV_AMP_COM) { _ni_com_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _ni_com_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_AC_DRIVE_COM) { _motor_com_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _motor_com_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_SPUMP_SPEED) { _spump_speed_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _spump_speed_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_SPUMP) { _spump_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _spump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_APUMP) { _apump_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _apump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_BPUMP) { _bpump_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _bpump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_CPUMP) { _cpump_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _cpump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_DPUMP) { _dpump_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _dpump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }

	if(error_code&ERROR_EPUMP) { _epump_error->set_from_icon_name("software-update-urgent", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); error=true; }
	else { _epump_error->set_from_icon_name("emblem-default", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON); }


	if(error)
		_graph_tab->set_current_page(GRAPH_INDEX::ERROR);
	else
		_graph_tab->set_current_page(GRAPH_INDEX::GRAPH);


	static unsigned int prev_error = 0;
	if(error_code!=prev_error && error_code==0)
	{
		_monitor_ni->set_markup(fmt::format("<span foreground=\"orange\">0</span>"));
		_monitor_ph->set_markup(fmt::format("<span foreground=\"orange\">0</span>"));
		_monitor_temp->set_markup(fmt::format("<span foreground=\"orange\">0</span>"));
	}
	prev_error = error_code;


}

void appwindow::set_data(PACKET_RESPONSE* data)
{
	if(data)
	{
		/*
		 * error window process
		 */
		error_process(data, data->get_error());
		pump_process(data->pump);
		state_process(data->status);
		mode_process(data->mode);

		//monitoring value update
		_monitor_mto->set_markup(fmt::format("<span foreground=\"orange\">{}</span>",data->get_mto()));
		_monitor_current_ph->set_text(fmt::format("{}",data->get_ph()));
		_monitor_current_ni->set_text(fmt::format("{}",data->get_ni_gram()));
		_monitor_current_temp->set_text(fmt::format("{}",data->get_temperature()));
		_monitor_ni_sensor->set_text(fmt::format("{}",data->get_ni_sensor_dc()));

		//graph update
		_graph_impl->push_data("Ni", data->get_ni_gram());
		_graph_impl->push_data("pH", data->get_ph()*0.1);
	}

}

void appwindow::set_com_error()
{
	error_process(nullptr, ERROR_MB_COM);
}

void appwindow::set_profile(plunframework::interface::iprofile* profile)
{

}

void appwindow::on_sw_mode_changed(const Gtk::StateType& style)
{
	unsigned char mode = MODE_MANUAL;

	if(_sw_auto_mode->get_active())
		mode = MODE_AUTO;
	else
		mode = MODE_MANUAL;

	if(_request_packet.mode!=mode)
	{
		_request_packet.set_mode(mode);
		send_request();
	}

}


void appwindow::send_request()
{
	if(_property.find("pub")!=_property.end() && _property.find("name")!=_property.end())
	{
		char* req = (char*)_request_packet.get_packet();
		vector<char> data(req, req+sizeof(_request_packet));
		plunframework::broker::component_broker::get()->publish(_property["name"].c_str(),_property["pub"].c_str(), "raw", data);
	}
}

