/*
 * log.cpp
 *
 *  Created on: 2015. 3. 12.
 *      Author: hwang-linux
 */

#include "log.hpp"
#include "../../include/core/component_broker.hpp"
#include <boost/filesystem.hpp>
#include <chrono>
#include <ctime>


namespace plunframework {
namespace component {

namespace spd = spdlog;

COMPONENT_INSTANCE(log)
COMPONENT_CREATE(log)
COMPONENT_DESTROY

log::log()
:icomponent_assist(COMPONENT(log)) {
}

log::~log() {


}

bool log::setup(error::errorcode& e, int argc=0, char** argv=nullptr)
{
	_topic = get_profile()->get(profile::section::info, "pub").asString("log");
	string name = get_profile()->get(profile::section::info, "name").asString("log");
	broker::component_broker::get()->register_topic(this, _topic.c_str());

	string file = get_profile()->get(profile::section::property, "outfile").asString("noname.txt");
	string format = get_profile()->get(profile::section::property, "format").asString("%v");

	fs.open(file,std::ios::out|ios::app);

	//if you want to support streaming out to file.. see the reference document.
	try {
		_file_logger = spd::stdout_logger_mt(file);
		//_file_logger->set_pattern(format);
		_file_logger->info("This is another message with custom format");

		//std::chrono::time_point<std::chrono::system_clock> cur = std::chrono::high_resolution_clock::now();//std::chrono::system_clock::now();
		//std::time_t cur_time = std::chrono::system_clock::to_time_t(cur);

		//get_logger()->info("{}",std::ctime(&cur_time));

		//auto timeSinceEpoch = std::chrono::duration_cast(std::chrono::system_clock::now().time_since_epoch()).count();

		fs << "test2";
		fs.close();

	}
	catch(const spd::spdlog_ex& ex)
	{
		get_logger()->error("Cannot create logger instance");
	}

	return true;
}

bool log::run(error::errorcode& e)
{
	return true;
}

bool log::stop(error::errorcode& e)
{
	return true;
}

void log::request(message::message* msg)
{
	std::tuple<string, string> unpacked;

	if(message::unpack(&unpacked, *msg)==error::errorcode::PLUN_NO_ERROR) {
		try
		{
			if(std::get<0>(unpacked)=="info") { get_logger()->info(std::get<1>(unpacked)); }
			else if(std::get<0>(unpacked)=="trace") { get_logger()->trace(std::get<1>(unpacked)); }
			else if(std::get<0>(unpacked)=="debug") { get_logger()->debug(std::get<1>(unpacked)); }
			else if(std::get<0>(unpacked)=="error") { get_logger()->error(std::get<1>(unpacked)); }
			else if(std::get<0>(unpacked)=="warning") { get_logger()->warn(std::get<1>(unpacked)); }
			else if(std::get<0>(unpacked)=="critical") { get_logger()->critical(std::get<1>(unpacked)); }
			else if(std::get<0>(unpacked)=="alert") { get_logger()->alert(std::get<1>(unpacked)); }
			else if(std::get<0>(unpacked)=="emerg") { get_logger()->emerg(std::get<1>(unpacked)); }
			else if(std::get<0>(unpacked)=="notice") { get_logger()->notice(std::get<1>(unpacked)); }
			else { get_logger()->info(std::get<1>(unpacked)); }
		}
		catch(const spd::spdlog_ex& ex)
		{
		}
	}
}

} /* namespace util */
} /* namespace plunframework */
