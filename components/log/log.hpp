/*
 * log.hpp
 *
 *  Created on: 2015. 3. 12.
 *      Author: Byunghun Hwang<bhhwang@synapse.com>
 *
 *      Simple Log component
 */

#ifndef LOG_HPP_
#define LOG_HPP_

#include "../../include/core/interface/icomponent_assist.hpp"
#include <memory>
#include <fstream>

using namespace std;

namespace plunframework {
namespace component {


class log : public interface::icomponent_assist {
public:
	log();
	virtual ~log();

	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

private:
	string _topic;
	ofstream fs;
};

COMPONENT_EXPORT

} /* namespace component */
} /* namespace plunframework */

#endif /* LOG_HPP_ */
