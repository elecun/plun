/*
 * httpserver.hpp
 *
 *  Created on: 2015. 5. 26.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Httpserver using libmicrohttpd
 */

#ifndef HTTPSERVER_HPP_
#define HTTPSERVER_HPP_

#include "../../include/core/interface/icomponent_service.hpp"

namespace plunframework {
namespace component {

class httpserver : public interface::icomponent_service {
public:
	httpserver();
	virtual ~httpserver();

	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

	bool loop();
};

} /* namespace component */
} /* namespace plunframework */

#endif /* HTTPSERVER_HPP_ */
