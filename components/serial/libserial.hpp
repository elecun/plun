/*
 * libserial.hpp
 *
 *  Created on: 2015. 1. 27.
 *      Author: Byunghun Hwang <bhhwang@nsynapse.com>
 *
 *      Serial Communication Library
 */

#ifndef LIBSERIAL_HPP_
#define LIBSERIAL_HPP_

#include <sys/types.h>
#include <termios.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>


class libserial {
public:
	libserial();
	virtual ~libserial();

	bool open(const char* port, int baudrate);
	void close();

	int write(const char* data, int size);
	int write(const unsigned char* data, int size);

	int read(unsigned char* pdata, int size);

private:
	int _device;
	struct termios _tio;
};

#endif /* LIBSERIAL_HPP_ */
