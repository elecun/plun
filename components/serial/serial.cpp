/*
 * serial.cpp
 *
 *  Created on: 2015. 1. 27.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 */


#include "serial.hpp"
#include <string>
#include <cstring>
#include "../../include/core/component_broker.hpp"
#include "../../include/extra/format.h"
#include "../../include/extra/spdlog/spdlog.h"
#include <array>

using namespace std;

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(serial)
COMPONENT_CREATE(serial)
COMPONENT_DESTROY


serial::serial()
:interface::icomponent_assist(COMPONENT(serial)), _serial(nullptr) {


}

serial::~serial() {

	if(_serial)
		delete _serial;
}

bool serial::setup(error::errorcode& e, int argc=0, char** argv=nullptr)
{
	string port = get_profile()->get(profile::section::property, "port").asString("/dev/ttyS0");
	int baudrate = get_profile()->get(profile::section::property, "baudrate").asInt(19200);
	_topic = get_profile()->get(profile::section::info, "pub").asString("communication/serial");

	get_logger()->info("pub topic : {}", _topic);
	get_logger()->info("serial port : {}", port);
	get_logger()->info("baudrate : {}", baudrate);

	if(_serial==nullptr)
		_serial = new libserial();

	if(!_serial->open(port.c_str(), baudrate))
	{
		if(_serial!=nullptr)
		{
			delete _serial;
			_serial = nullptr;
		}
		get_logger()->error("Cannot open the serial port {}", port);
		return false;
	}

	return true;
}

bool serial::run(error::errorcode& e)
{
	e = error::errorcode::PLUN_NO_ERROR;
	if(!_read_task)
		_read_task = create_task(component::serial::read_task);

	return true;
}

bool serial::stop(error::errorcode& e)
{
	e = error::errorcode::PLUN_NO_ERROR;

	destroy_task(_read_task);
	if(_serial)
	{
		_serial->close();

		delete _serial;
		_serial = nullptr;
	}

	return true;
}

void serial::request(message::message* msg)
{
	std::tuple<string, vector<char>> unpacked_char;

	if(message::unpack(&unpacked_char, *msg)==error::errorcode::PLUN_NO_ERROR) {
		vector<char> data = std::get<1>(unpacked_char);

		if(std::get<0>(unpacked_char)=="string")
		{
			_serial->write(&data[0], data.size());
			string raw = string(data.begin(), data.end());
			get_logger()->info("Received {} bytes message : {}", data.size(), raw);
		}
		else if(std::get<0>(unpacked_char)=="raw") {
			_serial->write((unsigned char*)&data[0], data.size());
			print_hex((unsigned char*)&data[0], data.size());
			get_logger()->info("Received {} bytes message", data.size());
		}
	}
	else
		get_logger()->info("Message cannot be unpacked.");
}


void serial::read_task()
{
	while(1)
	{
		try
		{
			if(_serial)
			{
				const unsigned int len = 1024;
				unsigned char* buffer = new unsigned char[len];
				int readsize = _serial->read(buffer, sizeof(unsigned char)*len);

				if(readsize>0) {
					print_hex(buffer, readsize);

					vector<unsigned char> data;
					data.assign(buffer, buffer+readsize*sizeof(unsigned char));
					broker::component_broker::get()->publish(this, _topic.c_str(), "raw", data);
				}

				delete []buffer;
			}

			boost::this_thread::sleep(posix_time::milliseconds(1));
		}
		catch(thread_interrupted&)
		{
			break;
		}
	}
}

void serial::print_hex(unsigned char* data, int len)
{
	string res="";
	char const hex[16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	for(int i=0;i<len; ++i)
	{
		const char c = data[i];
		res.append(&hex[(c&0xf0)>>4], 1);
		res.append(&hex[c&0x0f],1);
		res.append(" ");
	}

	get_logger()->info("Received Data : {}", res);

}

} /* namespace component */
} /* namespace plunframework */


