/*
 * serial.hpp
 *
 *  Created on: 2015. 1. 27.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Serial Communication Component(Service component)
 */

#ifndef SERIAL_HPP_
#define SERIAL_HPP_

#include "../../include/core/interface/icomponent_assist.hpp"
#include "libserial.hpp"
#include <queue>

using namespace std;

namespace plunframework {
namespace component {

class serial : public interface::icomponent_assist {
public:
	serial();
	virtual ~serial();

	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

private:
	int write(const char* data, int len);
	int write(const unsigned char* data, int leb);
	int read(char* data, int len);

	void print_hex(unsigned char* data, int len);

	/*
	 * tasks
	 */
	void read_task();

private:
	libserial* _serial;
	process::task _read_task;
	string _topic;
};

COMPONENT_EXPORT

} /* namespace component */
} /* namespace plunframework */

#endif /* SERIAL_HPP_ */
