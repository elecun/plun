/*
 * naaftcpadapter.cpp
 *
 *  Created on: 2015. 6. 5.
 *      Author: hwang-linux
 */

#include "naaftcpadapter.hpp"
#include "../../include/core/component_broker.hpp"
#include <vector>

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(naaftcpadapter)
COMPONENT_CREATE(naaftcpadapter)
COMPONENT_DESTROY

naaftcpadapter::naaftcpadapter()
:interface::icomponent_assist(COMPONENT(naaftcpadapter)){
	// TODO Auto-generated constructor stub

}

naaftcpadapter::~naaftcpadapter() {
	// TODO Auto-generated destructor stub
}

bool naaftcpadapter::setup(error::errorcode& e, int argc, char** argv)
{
	_topic = get_profile()->get(profile::section::info, "pub").asString("protocol/naaftcpfileter");

	return true;
}

bool naaftcpadapter::run(error::errorcode& e)
{
	return true;
}

bool naaftcpadapter::stop(error::errorcode& e)
{
	return true;
}

void naaftcpadapter::request(message::message* msg)
{
	std::tuple<string, vector<unsigned char>> unpacked_data;
	if(message::unpack(&unpacked_data, *msg)==error::errorcode::PLUN_NO_ERROR) {
		if(std::get<0>(unpacked_data)=="protocol") {
			vector<unsigned char> data = std::get<1>(unpacked_data);

			//publish
			broker::component_broker::get()->publish(this, _topic.c_str(), "broadcast", data);
			get_logger()->info("Published message via {}",_topic);

		}
		else {
			get_logger()->error("Undefined Message was received.");
		}
	}
}

} /* namesppace component */
} /* namespace plunframework */
