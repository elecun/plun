/*
 * naaftcpadapter.hpp
 *
 *  Created on: 2015. 6. 5.
 *      Author: hwang-linux
 */

#ifndef NAAFTCPADAPTER_HPP_
#define NAAFTCPADAPTER_HPP_

#include "../../include/core/interface/icomponent_assist.hpp"

namespace plunframework {
namespace component {

class naaftcpadapter : public interface::icomponent_assist {
public:
	naaftcpadapter();
	virtual ~naaftcpadapter();

	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

private:
	string _topic;
};

COMPONENT_EXPORT

} /* namespace component */

} /* namespace plunframework */

#endif /* NAAFTCPADAPTER_HPP_ */
