/*
 * session.hpp
 *
 *  Created on: 2015. 1. 27.
 *      Author: Byunghun Hwang <bhhwang@nsynapse.com>
 *
 *      TCP Server Session
 */

#ifndef SESSION_HPP_
#define SESSION_HPP_

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <deque>
#include <boost/array.hpp>
#include <string>
#include <memory>
#include <boost/optional.hpp>
#include "typedef.hpp"
#include "../../include/core/interface/icomponent_base.hpp"

using namespace std;
using namespace boost;
using namespace plunframework::interface;

#define MAX_RECEIVE_BUFFER_SIZE		2048

class tcp_session {
public:
	tcp_session(session_uuid id, boost::asio::io_service& io_service, icomponent_base* comp);
	virtual ~tcp_session();

	session_uuid get_session_id() { return _session_uuid; }
	boost::asio::ip::tcp::socket& socket() { return _socket; }

	void receive();
	void send(unsigned char* pData, const int len, const bool priority=true);
	void init();

private:
	void handle_send(const boost::system::error_code& error, size_t bytes_transferred);
	void handle_receive(const boost::system::error_code& error, size_t bytes_transferred);

private:
	session_uuid _session_uuid;
	boost::asio::ip::tcp::socket _socket;
	optional<int> _keepalive_timeout;
	std::shared_ptr<spdlog::logger> _logger;
	boost::array<unsigned char, MAX_RECEIVE_BUFFER_SIZE> _read_buffer;
	deque<unsigned char*> _sendQueue;
	icomponent_base* _comp = nullptr;
};

#endif /* SESSION_HPP_ */
