
#include "sessionmanager.hpp"

session_manager* session_manager::_instance = nullptr;

session_manager* session_manager::get()
{
	if(!_instance)
		_instance = new session_manager;
	return _instance;
}



void session_manager::destroy() {
	for(auto session:_session_container) {
		session.second->socket().close();
		delete session.second;
	}
	_session_container.clear();
	delete _instance;
}

tcp_session* session_manager::get_session(session_uuid id) {
	auto ses = _session_container.find(id);
	if(ses!=_session_container.end())
		return ses->second;
	else
		return nullptr;
}

unsigned int session_manager::size() {
	return _session_container.size();
}

void session_manager::close_session(const session_uuid id) {
	close(_session_container.find(id));
}

void session_manager::insert(session_uuid& id, tcp_session* session) {
	_session_container.insert(pair<session_uuid, tcp_session*>(id, session));
}

void session_manager::generate_session_id(session_uuid& id)
{
	uuid_t	guid;
	uuid_generate(guid);	//16byte

	char ch_guid[64];
	memset(ch_guid, 0x00, sizeof(ch_guid));
	uuid_unparse(guid, ch_guid);
	id = string(ch_guid);
}


void session_manager::close(session_container::iterator itr)
{
	if(itr!=_session_container.end())
	{
		if(itr->second->socket().is_open())
			itr->second->socket().close();

		delete itr->second;
		_session_container.erase(itr);
	}
}

/*
 * send only the connected client
 */
void session_manager::broadcast(unsigned char* data, int size)
{
	for(auto session:_session_container) {
		if(session.second->socket().is_open()) {
			session.second->send(data, size, true);
		}
	}
}

