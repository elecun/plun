/*
 * sessionmanager.hpp
 *
 *  Created on: 2015. 6. 3.
 *      Author: byunghun Hwang <bhhwang@nsynapse.com>
 */

#ifndef SESSIONMANAGER_HPP_
#define SESSIONMANAGER_HPP_

#include "typedef.hpp"
#include <string>
#include "session.hpp"
#include <uuid/uuid.h>
#include <boost/unordered_map.hpp>

using namespace std;

typedef boost::unordered_map<session_uuid, tcp_session*> session_container;

class session_manager {
public:
	static session_manager* get();
	void destroy();
	tcp_session* get_session(session_uuid id);

	unsigned int size();
	void close_session(const session_uuid id);
	void insert(session_uuid& id, tcp_session* session);
	void generate_session_id(session_uuid& id);

	void broadcast(unsigned char* data, int size);

private:
	void close(session_container::iterator itr);

private:
	static session_manager* _instance;
	session_container _session_container;

};

#endif /* SESSIONMANAGER_HPP_ */
