/*
 * tcpserver.hpp
 *
 *  Created on: 2015. 1. 27.
 *      Author: Byunghun Hwang <bhhwang@nsynapse.com>
 *
 *      TCP Server component (available for multiple session)
 */

#ifndef TCPSERVER_HPP_
#define TCPSERVER_HPP_

#include "../../include/core/interface/icomponent_assist.hpp"
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <string>

using namespace std;

class tcp_session;
namespace plunframework {
namespace component {

class tcpserver : public interface::icomponent_assist {
public:
	tcpserver();
	virtual ~tcpserver();

	/*
	 * common interface
	 */
	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

	bool process(unsigned char* data, int size);

private:
	/*
	 * server functions
	 */
	void accept();
	void listen();
	void handle_accept(tcp_session* session, const boost::system::error_code& error);

private:
	boost::asio::io_service _io_service;
	boost::asio::ip::tcp::acceptor _acceptor;
	process::task _server_task;
	string _host;
	string _topic;
	unsigned int _port = 9090;

};

COMPONENT_EXPORT

} /* namespace component */
} /* namespace plunframework */

#endif /* TCPSERVER_HPP_ */
