/*
 * tcpserver.cpp
 *
 *  Created on: 2014. 11. 19.
 *      Author: Byunghun Hwang <bhhwang@nsynapse.com>
 *
 *      TCP Server Component
 */

#include "tcpserver.hpp"
#include "session.hpp"
#include "../../include/core/component_broker.hpp"
#include "sessionmanager.hpp"

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(tcpserver)
COMPONENT_CREATE(tcpserver)
COMPONENT_DESTROY

tcpserver::tcpserver()
:interface::icomponent_assist(COMPONENT(tcpserver)),_acceptor(boost::asio::ip::tcp::acceptor(_io_service)) {

}

tcpserver::~tcpserver() {
	session_manager::get()->destroy();
}

bool tcpserver::setup(error::errorcode& e, int argc, char** argv)
{
	_port = get_profile()->get(profile::section::property, "port").asUInt(9090);
	_host = get_profile()->get(profile::section::property, "hostip").asString("127.0.0.1");
	_topic = get_profile()->get(profile::section::info, "pub").asString("communication/tcp");

	return true;
}

bool tcpserver::run(error::errorcode& e)
{
	if(_acceptor.is_open())
		_acceptor.close();

	try {
		asio::ip::address ipaddr = asio::ip::address_v4::from_string(_host);
		_acceptor.open(asio::ip::tcp::endpoint(ipaddr, _port).protocol());
		_acceptor.bind(asio::ip::tcp::endpoint(ipaddr, _port));
	} catch (boost::system::error_code& e) {
		get_logger()->error("Server Error : {}", e.message());
	}

	if(!_server_task)
		_server_task = create_task(tcpserver::listen);

	return true;
}

bool tcpserver::stop(error::errorcode& e)
{
	_io_service.stop();

	if(_acceptor.is_open())
		_acceptor.close();

	destroy_task(_server_task);
	_server_task = nullptr;

	return true;
}

void tcpserver::listen()
{
	get_logger()->info("Start TCP Server {}:{}", _host, _port);

	accept();
	_io_service.run();
}

void tcpserver::request(message::message* msg)
{
	std::tuple<string, vector<unsigned char>> unpacked_data;
	if(message::unpack(&unpacked_data, *msg)==error::errorcode::PLUN_NO_ERROR) {
		if(std::get<0>(unpacked_data)=="broadcast") {
			vector<unsigned char> data = std::get<1>(unpacked_data);

			get_logger()->info("Broadcast");
			session_manager::get()->broadcast(data.data(), data.size());

			//publish
			//broker::component_broker::get()->publish(this, _topic.c_str(), "raw_send", data);

		}
		else if(std::get<0>(unpacked_data)=="write") {

		}
		else {
			get_logger()->error("Undefined Message was received.");
		}
	}
}

void tcpserver::accept()
{
	_acceptor.listen();

	session_uuid id;
	session_manager::get()->generate_session_id(id);
	session_manager::get()->insert(id, new tcp_session(id, _acceptor.get_io_service(), this));

	_acceptor.async_accept(session_manager::get()->get_session(id)->socket(),
								boost::bind(&tcpserver::handle_accept, this,
								session_manager::get()->get_session(id), boost::asio::placeholders::error));
}

void tcpserver::handle_accept(tcp_session* session, const boost::system::error_code& error)
{
	if(!error)
	{
		asio::ip::tcp::endpoint client = session->socket().remote_endpoint();
		get_logger()->info("({}) Accepted[{}] from {}:{}", session_manager::get()->size(), session->get_session_id(), client.address().to_string(), client.port());

		session->init();
		session->receive();

		accept();
	}
	else
	{
		get_logger()->error("error : {}",error.message());

		session_manager::get()->close_session(session->get_session_id());
	}
}

//only publish
bool tcpserver::process(unsigned char* data, int size)
{
	vector<unsigned char> rdata;
	rdata.assign(data, data+size*sizeof(unsigned char));

	broker::component_broker::get()->publish(this, _topic.c_str(), "raw", rdata);

	return false;
}


} /* namespace component */
} /* namespace plunframework */
