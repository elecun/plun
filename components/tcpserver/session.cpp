/*
 * session.cpp
 *
 *  Created on: 2015. 1. 27.
 *      Author: hwang-linux
 */

#include "session.hpp"
#include "tcpserver.hpp"
#include "../../include/core/component_broker.hpp"
#include "sessionmanager.hpp"

namespace spd = spdlog;

tcp_session::tcp_session(session_uuid id, asio::io_service& io_service, icomponent_base* comp)
:_session_uuid(id), _socket(io_service), _keepalive_timeout(3), _comp(comp) {

	try { _logger = spd::stdout_logger_mt(id); }
	catch(const spd::spdlog_ex& ex) { }
}

tcp_session::~tcp_session() {
	while(!_sendQueue.empty())
	{
		delete[] _sendQueue.front();
		_sendQueue.pop_front();
	}
}

void tcp_session::init()
{

	int native_fd = _socket.native();
	int timeout = *_keepalive_timeout;
	int intvl = 1;
	int probes = 3;
	int on = 1;

	int ret_keepalive = setsockopt(native_fd, SOL_SOCKET, SO_KEEPALIVE, (void*) &on, sizeof(int));
	int ret_keepidle = setsockopt(native_fd, SOL_TCP, TCP_KEEPIDLE, (void*) &timeout, sizeof(int));
	int ret_keepintvl = setsockopt(native_fd, SOL_TCP, TCP_KEEPINTVL, (void*) &intvl, sizeof(int));
	int ret_keepinit = setsockopt(native_fd, SOL_TCP, TCP_KEEPCNT, (void*) &probes, sizeof(int));

	if(ret_keepalive || ret_keepidle || ret_keepintvl || ret_keepinit)
	{
		//LOG_ERROR("Failed to enable keep alive on TCP client socket!");
	}
}

/*
 * received data process
 */
void tcp_session::receive()
{
	_socket.async_read_some(
		asio::buffer(_read_buffer),
		boost::bind(&tcp_session::handle_receive, this,
				asio::placeholders::error,
				asio::placeholders::bytes_transferred));
}

void tcp_session::send(unsigned char* pData, const int len, const bool priority)
{
	unsigned char* pSendData = nullptr;

	if(!priority)
	{
		pSendData = new unsigned char[len];
		memcpy(pSendData, pData, len);
		_sendQueue.push_back(pSendData);
	}
	else
		pSendData = pData;

	if(!priority && _sendQueue.size()>1)
		return;

	asio::async_write(_socket,
						asio::buffer(pSendData, len),
						boost::bind(&tcp_session::handle_send, this,
						asio::placeholders::error,
						asio::placeholders::bytes_transferred));

}

void tcp_session::handle_send(const boost::system::error_code& error, size_t bytes_transferred)
{
	if(!_sendQueue.empty())
	{
		delete[] _sendQueue.front();
		_sendQueue.pop_front();
	}

	if(!_sendQueue.empty())
	{
		unsigned char* pData = _sendQueue.front();
		send(pData, _sendQueue.size(), true);
	}
}

void tcp_session::handle_receive(const boost::system::error_code& error, size_t bytes_transferred)
{
	if(error)
	{
		switch(error.value())
		{
		case 2:	_logger->info("Disconnected.");	break;	//end of file (disconnection)
		default:
			_logger->error("{}:{}",error.value(), error.message());
		}
		session_manager::get()->close_session(_session_uuid);
	}
	else
	{

		unsigned char* rbuf = new unsigned char[bytes_transferred];
		std::copy(_read_buffer.data(), _read_buffer.data()+sizeof(unsigned char)*bytes_transferred, rbuf);
		_logger->info("{} bytes received. ", bytes_transferred);

		plunframework::component::tcpserver* server = dynamic_cast<plunframework::component::tcpserver*>(_comp);
		server->process(rbuf, bytes_transferred);

		delete []rbuf;

		receive();
	}
}
