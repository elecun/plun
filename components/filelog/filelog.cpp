/*
 * filelog.cpp
 *
 *  Created on: 2015. 6. 15.
 *      Author: hwang-linux
 */

#include "filelog.hpp"
#include "../../include/core/component_broker.hpp"
#include "include/format.h"
#include <boost/filesystem.hpp>
#include <ctime>
#include <sys/stat.h>

using namespace std;
using namespace boost;

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(filelog)
COMPONENT_CREATE(filelog)
COMPONENT_DESTROY

filelog::filelog()
:icomponent_assist(COMPONENT(filelog)){


}

filelog::~filelog() {
	// TODO Auto-generated destructor stub
}

bool filelog::setup(error::errorcode& e, int argc=0, char** argv=nullptr)
{
	_topic = get_profile()->get(profile::section::info, "pub").asString("log");
	_file = new std::ofstream;

	return true;
}

bool filelog::run(error::errorcode& e)
{
	return true;
}

bool filelog::stop(error::errorcode& e)
{
	if(_file->is_open())
	{
		_file->close();
		delete _file;
	}

	return true;
}

void filelog::request(message::message* msg)
{
	std::tuple<string, string> unpacked;

	if(message::unpack(&unpacked, *msg)==error::errorcode::PLUN_NO_ERROR)
	{
		//API : new
		if(std::get<0>(unpacked)=="newfile") {
			_filename = std::get<1>(unpacked);

			//extract path
			string fullpath = std::get<1>(unpacked);
			string path = fullpath.substr(0, fullpath.find_last_of("\\/"));

			//create path
			boost::filesystem::path dir(path);
			if(!(boost::filesystem::exists(dir)))
				boost::filesystem::create_directory(dir);

			_file->open(_filename, ios_base::out|ios_base::app);
			get_logger()->info("Create new log file {}",_filename);
		}

		//API : log
		else if(std::get<0>(unpacked)=="log") {

			if(_file) {
				if(_file->is_open()) {
					string log = std::get<1>(unpacked);
					*_file << log;
				}
				else
					get_logger()->error("{} file is not opened.", _filename);
			}
		}
		else if(std::get<0>(unpacked)=="timelog") {

			/*if(_file->is_open()) {
				string log = std::get<1>(unpacked);
				time_t now = time(0);
				tm *ltm = localtime(&now);
			}
			else
				get_logger()->error("{} file is not opened.", _filename);*/
		}
		else
			get_logger()->error("Undefined API {}",std::get<0>(unpacked));
	}
	else
		get_logger()->error("Cannot be unpacked the message.");
}

} /* namespace component */
} /* namespace plunframework */
