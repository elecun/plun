/*
 * filelog.hpp
 *
 *  Created on: 2015. 6. 15.
 *      Author: hwang-linux
 */

#ifndef FILELOG_HPP_
#define FILELOG_HPP_

#include "../../include/core/interface/icomponent_assist.hpp"
#include <fstream>
#include <string>

using namespace std;

namespace plunframework {
namespace component {

class filelog : public interface::icomponent_assist {
public:
	filelog();
	virtual ~filelog();

	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

private:
	string _topic;
	string _filename;
	std::ofstream* _file = nullptr;
};

COMPONENT_EXPORT

} /* namespace component */
} /* namespace plunframework */

#endif /* FILELOG_HPP_ */
