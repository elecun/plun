/*
 * mqttPub.hpp
 *
 *  Created on: 2015. 5. 22.
 *      Author: hwang-linux
 */

#ifndef MQTTPUB_HPP_
#define MQTTPUB_HPP_

namespace plunframework {

class mqttPub {
public:
	mqttPub();
	virtual ~mqttPub();
};

} /* namespace plunframework */

#endif /* MQTTPUB_HPP_ */
