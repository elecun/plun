/*
 * mqttSub.hpp
 *
 *  Created on: 2015. 5. 22.
 *      Author: hwang-linux
 */

#ifndef MQTTSUB_HPP_
#define MQTTSUB_HPP_

namespace plunframework {

class mqttSub {
public:
	mqttSub();
	virtual ~mqttSub();
};

} /* namespace plunframework */

#endif /* MQTTSUB_HPP_ */
