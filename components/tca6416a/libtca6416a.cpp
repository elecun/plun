/*
 * libtca6416a.cpp
 *
 *  Created on: 2015. 3. 28.
 *      Author: hwang-linux
 */

#include "libtca6416a.hpp"

#include <stdio.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdint.h>

unsigned char set_i2c_register(int file,
                            unsigned char addr,
                            unsigned char reg,
                            unsigned char value) {

    unsigned char outbuf[2];
    struct i2c_rdwr_ioctl_data packets;
    struct i2c_msg messages[1];

    messages[0].addr  = addr;
    messages[0].flags = 0;
    messages[0].len   = 2;
    messages[0].buf   = outbuf;

    /* The first byte indicates which register we'll write */
    outbuf[0] = reg;

    /*
     * The second byte indicates the value to write.  Note that for many
     * devices, we can write multiple, sequential registers at once by
     * simply making outbuf bigger.
     */

    outbuf[1] = value;

    /* Transfer the i2c packets to the kernel and verify it worked */
    packets.msgs  = messages;
    packets.nmsgs = 1;
    if(ioctl(file, I2C_RDWR, &packets) < 0) {
        perror("Unable to send data");
        return 1;
    }

    return 0;
}


unsigned char get_i2c_register(int file,
                            unsigned char addr,
                            unsigned char reg,
                            unsigned char *val) {

    unsigned char inbuf, outbuf;
    struct i2c_rdwr_ioctl_data packets;
    struct i2c_msg messages[2];

    /*
     * In order to read a register, we first do a "dummy write" by writing
     * 0 bytes to the register we want to read from.  This is similar to
     * the packet in set_i2c_register, except it's 1 byte rather than 2.
     */

    outbuf = reg;
    messages[0].addr  = addr;
    messages[0].flags = 0;
    messages[0].len   = 1;
    messages[0].buf   = &outbuf;

    /* The data will get returned in this structure */
    messages[1].addr  = addr;
    messages[1].flags = I2C_M_RD/* | I2C_M_NOSTART*/;
    messages[1].len   = 1;
    messages[1].buf   = &inbuf;

    /* Send the request to the kernel and get the result back */
    packets.msgs      = messages;
    packets.nmsgs     = 2;
    if(ioctl(file, I2C_RDWR, &packets) < 0) {
        perror("Unable to send data");
        return 1;
    }
    *val = inbuf;

    return 0;
}

libtca6416a::libtca6416a()
{
}

libtca6416a::~libtca6416a()
{
	if(_device>=0)
		close();
}

/*
 * device open
 */
bool libtca6416a::open(const char* device, unsigned char ic_addr)
{
	if((_device = ::open(device, O_RDWR))<0) {
		return false;
	}

	_ic_address = ic_addr;
	init();
	return true;
}

/*
 * close device
 */
bool libtca6416a::close()
{
	if(::close(_device)<0)
		return false;

	return true;
}


unsigned char libtca6416a::read(unsigned char port, unsigned char& state)
{
	unsigned char in_bank, current_bank_state;

	if((port > 7) && (port <= TCA6416A_MAX_PORT)) // bank 1
	{
		port -= 8; // shift GPIO number for bank 1
		in_bank = TCA6416A_INPUT_BANK_1;
	}
	else if((port >= 0) && (port < 8))
	{
		in_bank = TCA6416A_INPUT_BANK_0;
	}
	else
		return -1;

	get_i2c_register(_device, _ic_address, in_bank, &current_bank_state);

	if(current_bank_state & (1 << port))
	{
		state = current_bank_state;
		return 1;
	}
	else
		return 0;

}


/*
 * port : pre-defined port number
 * state : 1 is set
 */

unsigned char libtca6416a::out(unsigned char port, unsigned char state)
{
	unsigned char out_bank, *current_bank_state;

	if((port > 7) && (port <= TCA6416A_MAX_PORT)) // bank 1
	{
		port -= 8; // shift GPIO number for bank 1
		out_bank = TCA6416A_OUTPUT_BANK_1;
		current_bank_state =  &_input_regs_bank_1;
	}
	else if((port >= 0) && (port < 8))
	{
		out_bank = TCA6416A_OUTPUT_BANK_0;
		current_bank_state = &_input_regs_bank_0;
	}
	else
		return -1;

	if(state == 1)
	{
		*current_bank_state |= (1 << port);
		set_i2c_register(_device, _ic_address, out_bank, *current_bank_state);
	}
	else
	{
		*current_bank_state &= ~(1 << port);
		set_i2c_register(_device, _ic_address, out_bank, *current_bank_state);
	}

	return 0;

}


unsigned char libtca6416a::set(unsigned char port, unsigned char mode)
{
	unsigned char out_bank, *current_bank_state;

	if((port > 7) && (port <= TCA6416A_MAX_PORT)) // bank 1
	{
		port -= 8; // shift GPIO number for bank 1
		out_bank = TCA6416A_CONFIG_BANK_1;
		current_bank_state =  &_input_regs_bank_1;
	}
	else if((port >= 0) && (port < 8))
	{
		out_bank = TCA6416A_CONFIG_BANK_0;
		current_bank_state =  &_input_regs_bank_0;
	}
	else
		return -1;

	if(mode == TCA6416A_PORT_IN)
	{
		*current_bank_state |= (1 << port);
		set_i2c_register(_device, _ic_address, out_bank, *current_bank_state);
	}
	else if(mode == TCA6416A_PORT_OUT)
	{
		*current_bank_state &= ~(1 << port);
		set_i2c_register(_device, _ic_address, out_bank, *current_bank_state);
	}

	return 0;

}


unsigned char libtca6416a::init()
{
	if(_device>0)
	{
		get_i2c_register(_device, _ic_address, TCA6416A_INPUT_BANK_0, &_output_regs_bank_0);
		get_i2c_register(_device, _ic_address, TCA6416A_INPUT_BANK_1, &_output_regs_bank_1);
		get_i2c_register(_device, _ic_address, TCA6416A_POLARITY_BANK_0, &_polarity_regs_bank_0);
		get_i2c_register(_device, _ic_address, TCA6416A_POLARITY_BANK_1, &_polarity_regs_bank_1);
		get_i2c_register(_device, _ic_address, TCA6416A_CONFIG_BANK_0, &_config_regs_bank_0);
		get_i2c_register(_device, _ic_address, TCA6416A_CONFIG_BANK_1, &_config_regs_bank_1);
	}

	return 0;

}



