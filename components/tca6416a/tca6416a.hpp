/*
 * tca6416a.hpp
 *
 *  Created on: 2015. 3. 14.
 *      Author: hwang-linux
 */

#ifndef TCA6416A_HPP_
#define TCA6416A_HPP_


#include "../../include/core/interface/icomponent_assist.hpp"
#include "libtca6416a.hpp"

namespace plunframework {
namespace component {

class tca6416a : public interface::icomponent_assist {
public:
	tca6416a();
	virtual ~tca6416a();

	bool setup(error::errorcode& e);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);

	void request(message::message* msg);

private:
	libtca6416a* _lib = nullptr;
	const std::map<int, unsigned char> _portmap = {
			{0, TCA6416A_PORT0 },
			{1, TCA6416A_PORT1 },
			{2, TCA6416A_PORT2 },
			{3, TCA6416A_PORT3 },
			{4, TCA6416A_PORT4 },
			{5, TCA6416A_PORT5 },
			{6, TCA6416A_PORT6 },
			{7, TCA6416A_PORT7 },
			{8, TCA6416A_PORT8 },
			{9, TCA6416A_PORT9 },
			{10, TCA6416A_PORT10 },
			{11, TCA6416A_PORT11 },
			{12, TCA6416A_PORT12 },
			{13, TCA6416A_PORT13 },
			{14, TCA6416A_PORT14 },
			{15, TCA6416A_PORT15 }
	};
};

COMPONENT_EXPORT

} /* namespace component */
} /* namespace plunframework */

#endif /* TCA6416A_HPP_ */
