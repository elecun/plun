/*
 * tca6416a.cpp
 *
 *  Created on: 2015. 3. 14.
 *      Author: hwang-linux
 */

#include "tca6416a.hpp"
#include "../../include/plunframework.hpp"

#define HIGH	0
#define LOW	1

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(tca6416a)
COMPONENT_CREATE(tca6416a)
COMPONENT_DESTROY

tca6416a::tca6416a()
:interface::icomponent_assist(COMPONENT(tca6416a)) {

	_lib = new libtca6416a();

}

tca6416a::~tca6416a() {

	if(_lib)
		delete _lib;
}

bool tca6416a::setup(error::errorcode& e)
{
	string device = get_profile()->get(profile::section::property, "device").asString("/dev/i2c-4");
	unsigned char address = get_profile()->get(profile::section::property, "address").asUChar(0x20);
	string topic = get_profile()->get(profile::section::info, "topic").asString("device/tca6416a");

	get_logger()->info("device : {}", device);
	get_logger()->info("{} component topic : {}",this->get_name(), topic);

	broker::component_broker::get()->register_topic(this, topic.c_str());

	if(!_lib->open(device.c_str(), address))
	{
		get_logger()->error("Cannot open {}", device);

		delete _lib;
		_lib = nullptr;

		return false;
	}

	if(!_lib)
		return false;

	_lib->set(TCA6416A_PORT0, TCA6416A_PORT_IN);	//P0
	_lib->set(TCA6416A_PORT1, TCA6416A_PORT_IN);	//P1
	_lib->set(TCA6416A_PORT2, TCA6416A_PORT_IN);	//P2
	_lib->set(TCA6416A_PORT3, TCA6416A_PORT_IN);	//P3

	_lib->set(TCA6416A_PORT12, TCA6416A_PORT_OUT);	//P14
	_lib->set(TCA6416A_PORT13, TCA6416A_PORT_OUT);	//P15
	_lib->set(TCA6416A_PORT14, TCA6416A_PORT_OUT);	//P16
	_lib->set(TCA6416A_PORT15, TCA6416A_PORT_OUT);	//P17

	//default value
	_lib->out(TCA6416A_PORT12, LOW);
	_lib->out(TCA6416A_PORT13, HIGH);
	_lib->out(TCA6416A_PORT14, LOW);
	_lib->out(TCA6416A_PORT15, HIGH);


	return true;
}

bool tca6416a::run(error::errorcode& e)
{
	return true;
}

bool tca6416a::stop(error::errorcode& e)
{
	//set default value
	_lib->out(TCA6416A_PORT12, LOW);
	_lib->out(TCA6416A_PORT13, LOW);
	_lib->out(TCA6416A_PORT14, LOW);
	_lib->out(TCA6416A_PORT15, LOW);

	return true;
}

void tca6416a::request(message::message* msg)
{
	std::tuple<string, int, int> unpacked_write;
	std::tuple<string, int> unpacked_read;

	if(message::unpack(&unpacked_write, *msg)==error::errorcode::PLUN_NO_ERROR)
	{
		if(std::get<0>(unpacked_write)=="write")
		{
			int port = std::get<1>(unpacked_write);
			if(port>0 && port<=15)
			{
				if(_portmap.find(port) != _portmap.end())
				{
					unsigned char byte_port = _portmap.at(port);
					int value = std::get<2>(unpacked_write);
					if(value==1)
						_lib->out(byte_port, HIGH);
					else
						_lib->out(byte_port, LOW);
				}
				else
					get_logger()->error("Cannot find the port {}.",port);
			}
		}
		else
			get_logger()->error("Unrecognized command");
	}
	else if(message::unpack(&unpacked_read, *msg)==error::errorcode::PLUN_NO_ERROR)
	{
		if(std::get<0>(unpacked_read)=="read")
		{
			int port = std::get<1>(unpacked_read);
			if(port>0 && port<=15)
			{
				if(_portmap.find(port) != _portmap.end())
				{
					unsigned char byte_port = _portmap.at(port);
					unsigned char state = 0x00;

					_lib->read(byte_port, state);
				}
				else
					get_logger()->error("Cannot find the port {}.",port);
			}
		}
		else
			get_logger()->error("Unrecognized command");
	}
	else
		get_logger()->info("Message cannot be unpacked.");
}

} /* namespace component */
} /* namespace plunframework */
