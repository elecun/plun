/*
 * libtca6416a.hpp
 *
 *  Created on: 2015. 3. 28.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      TCA6416A Control Library
 */

#ifndef LIBTCA6416A_HPP_
#define LIBTCA6416A_HPP_

#include <stdint.h>

/*
 * below definitions depends on the circuit to be used.
 */
#define TCA6416A_PORT0	0
#define TCA6416A_PORT1	1
#define TCA6416A_PORT2	2
#define TCA6416A_PORT3	3
#define TCA6416A_PORT4	4
#define TCA6416A_PORT5	5
#define TCA6416A_PORT6	6
#define TCA6416A_PORT7	7
#define TCA6416A_PORT8	8
#define TCA6416A_PORT9	9
#define TCA6416A_PORT10 10
#define TCA6416A_PORT11 11
#define TCA6416A_PORT12 12
#define TCA6416A_PORT13 13
#define TCA6416A_PORT14 14
#define TCA6416A_PORT15	15

#define TCA6416A_INPUT_BANK_0 		0x0
#define TCA6416A_INPUT_BANK_1 		0x1
#define TCA6416A_OUTPUT_BANK_0 		0x2
#define TCA6416A_OUTPUT_BANK_1 		0x3
#define TCA6416A_POLARITY_BANK_0 	0x4
#define TCA6416A_POLARITY_BANK_1	0x5
#define TCA6416A_CONFIG_BANK_0 		0x6
#define TCA6416A_CONFIG_BANK_1		0x7

#define TCA6416A_PORT_IN	1
#define TCA6416A_PORT_OUT	0

#define TCA6416A_MAX_PORT 15


unsigned char set_i2c_register(int file, unsigned char addr, unsigned char reg, unsigned char value);
unsigned char get_i2c_register(int file, unsigned char addr, unsigned char reg, unsigned char *val);

class libtca6416a {
public:
	libtca6416a();
	virtual ~libtca6416a();

	/*
	 * device open/close
	 */
	bool open(const char* device, unsigned char ic_addr);
	bool close();

	/*
	 * control
	 */

	unsigned char read(unsigned char port, unsigned char& state);
	unsigned char out(unsigned char port, unsigned char state);
	unsigned char set(unsigned char port, unsigned char mode);

private:
	unsigned char init();

private:
	int _device = -1;
	unsigned char _ic_address = 0x00;

	unsigned char _input_regs_bank_0 = 0x00;
	unsigned char _input_regs_bank_1 = 0x00;
	unsigned char _output_regs_bank_0 = 0x00;
	unsigned char _output_regs_bank_1 = 0x00;
	unsigned char _polarity_regs_bank_0 = 0x00;
	unsigned char _polarity_regs_bank_1 = 0x00;
	unsigned char _config_regs_bank_0 = 0x00;
	unsigned char _config_regs_bank_1 = 0x00;

};

#endif /* LIBTCA6416A_HPP_ */
