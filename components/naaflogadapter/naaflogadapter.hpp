/*
 * naaflogadapter.hpp
 *
 *  Created on: 2015. 6. 16.
 *      Author: hwang-linux
 */

#ifndef NAAFLOGADAPTER_HPP_
#define NAAFLOGADAPTER_HPP_

#include "../../include/core/interface/icomponent_assist.hpp"
#include <string>

namespace plunframework {
namespace component {

class naaflogadapter : public interface::icomponent_assist {
public:
	naaflogadapter();
	virtual ~naaflogadapter();

	bool setup(error::errorcode& e, int argc, char** argv);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	void request(message::message* msg);

private:
	string _topic;
	string _filename;
	string _prefix;
};

COMPONENT_EXPORT

}
} /* namespace plunframework */

#endif /* NAAFLOGADAPTER_HPP_ */
