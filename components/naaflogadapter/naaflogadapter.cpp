/*
 * naaflogadapter.cpp
 *
 *  Created on: 2015. 6. 16.
 *      Author: hwang-linux
 */

#include "naaflogadapter.hpp"
#include "../../include/core/component_broker.hpp"
#include "include/format.h"
#include "protocol.hpp"
#include <ctime>

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(naaflogadapter)
COMPONENT_CREATE(naaflogadapter)
COMPONENT_DESTROY

naaflogadapter::naaflogadapter()
:icomponent_assist(COMPONENT(naaflogadapter)){
	// TODO Auto-generated constructor stub

}

naaflogadapter::~naaflogadapter() {
	// TODO Auto-generated destructor stub
}

bool naaflogadapter::setup(error::errorcode& e, int argc=0, char** argv=nullptr)
{
	_topic = get_profile()->get(profile::section::info, "pub").asString("log/naaflogadapter");
	_prefix = get_profile()->get(profile::section::property, "prefix").asString("./");

	return true;
}

bool naaflogadapter::run(error::errorcode& e)
{
	return true;
}

bool naaflogadapter::stop(error::errorcode& e)
{
	return true;
}

void naaflogadapter::request(message::message* msg)
{
	std::tuple<string, vector<unsigned char>> unpacked_data;
	if(message::unpack(&unpacked_data, *msg)==error::errorcode::PLUN_NO_ERROR) {
		if(std::get<0>(unpacked_data)=="protocol") {
			vector<unsigned char> data = std::get<1>(unpacked_data);

			PACKET_RESPONSE* packet = (PACKET_RESPONSE*)data.data();

			//process log format
			time_t now = time(0);
			tm *ltm = localtime(&now);

			string f = fmt::format("{}{}-{}-{} {}-{}-{}.txt",_prefix,(1900+ltm->tm_year), (1+ltm->tm_mon), (ltm->tm_mday), (ltm->tm_hour), 0,0);
			if(_filename.empty() || _filename.compare(f)!=0)
			{
				_filename = f;
				broker::component_broker::get()->publish(this, _topic.c_str(), "newfile", _filename);
			}


			string log = fmt::format("[{}-{}-{} {}:{}:{}]\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n",
					(1900+ltm->tm_year), (1+ltm->tm_mon), (ltm->tm_mday), (ltm->tm_hour), (ltm->tm_min),(ltm->tm_sec),
					packet->get_ni_gram(), packet->get_ni_sensor_dc(), packet->get_ph(), packet->get_mto(), 0,0,0,0,0,0,"1111111");

			//publish
			broker::component_broker::get()->publish(this, _topic.c_str(), "log", log);
			get_logger()->info("Published message via {}",_topic);

		}
		else {
			get_logger()->error("Undefined Message was received.");
		}
	}
}

}
} /* namespace plunframework */
