/*
 * appwindow.cpp
 *
 *  Created on: 2015. 3. 29.
 *      Author: hwang-linux
 */

#include "appwindow.hpp"

appwindow::appwindow(BaseObjectType* obj, const Glib::RefPtr<Gtk::Builder>& glade)
:Gtk::Window(obj),_builder(glade),_main_window(nullptr){

	_builder = Gtk::Builder::create();
}

appwindow::~appwindow() {
	// TODO Auto-generated destructor stub
}


bool appwindow::open(const char* ui_path)
{
	return _builder->add_from_file(ui_path);
}
