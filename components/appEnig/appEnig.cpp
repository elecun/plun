/*
 * appEnig.cpp
 *
 *  Created on: 2015. 3. 12.
 *      Author: hwang-linux
 */

#include "appEnig.hpp"

namespace plunframework {
namespace component {

COMPONENT_INSTANCE(appEnig)
COMPONENT_CREATE(appEnig)
COMPONENT_DESTROY

appEnig::appEnig()
:icomponent_app(COMPONENT(appEnig)), _window(nullptr){

	_app = Gtk::Application::create();
	_builder = Gtk::Builder::create();

}

appEnig::~appEnig() {
	if(_window)
		delete _window;

}

bool appEnig::setup(error::errorcode& e)
{
	if(!_builder->add_from_file("./gui/enig.glade"))
		std::cout << "open failed" << std::endl;

	_builder->get_widget_derived("id_top_window", _window);

	/*if(!_window)
	{
		_window = new appwindow("plun.app.enig");
		if(!_window->open("./gui/enig.glade")) {
			e = error::errorcode::PLUN_RUN_FAIL;
			std::cout << "open" << std::endl;
		}
		else
			std::cout << "open failed" << std::endl;
	}*/

	return true;
}

bool appEnig::run(error::errorcode& e)
{
	/*if(_window)
		_app->run(*_window);*/

	return true;
}

bool appEnig::stop(error::errorcode& e)
{
	if(_window)
	{
		delete _window;
		_window = nullptr;
	}

	return true;
}
bool appEnig::loop()
{
	return true;
}

void appEnig::request(message::message* msg)
{

}

} /* namespace component */
} /* namespace plunframework */
