################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../appEnig.cpp \
../appwindow.cpp 

OBJS += \
./appEnig.o \
./appwindow.o 

CPP_DEPS += \
./appEnig.d \
./appwindow.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -I/usr/local/include -I/usr/include -O0 -g3 -Wall -c -fmessage-length=0 $(shell pkg-config --cflags gtkmm-3.0) -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


