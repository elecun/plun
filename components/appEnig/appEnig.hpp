/*
 * appEnig.hpp
 *
 *  Created on: 2015. 3. 12.
 *      Author: Byunghun Hwang<bhhwang@nsynapse.com>
 *
 *      Application component for Enig Project
 */

#ifndef APPENIG_HPP_
#define APPENIG_HPP_

#include "../../include/core/interface/icomponent_app.hpp"
#include "appwindow.hpp"

namespace plunframework {
namespace component {

class appEnig : public interface::icomponent_app {
public:
	appEnig();
	virtual ~appEnig();

	bool setup(error::errorcode& e);
	bool run(error::errorcode& e);
	bool stop(error::errorcode& e);
	bool loop();

	void request(message::message* msg);

private:
	appwindow* _window;

	Glib::RefPtr<Gtk::Application> _app;
	Glib::RefPtr<Gtk::Builder> _builder;
};

COMPONENT_EXPORT

} /* namespace component */
} /* namespace plunframework */

#endif /* APPENIG_HPP_ */
