/*
 * appwindow.hpp
 *
 *  Created on: 2015. 3. 29.
 *      Author: app windows
 */

#ifndef APPWINDOW_HPP_
#define APPWINDOW_HPP_

#include <gtkmm-3.0/gtkmm.h>
#include <cairomm-1.0/cairomm/cairomm.h>

class appwindow  : public Gtk::Window {
public:
	appwindow(BaseObjectType* obj, const Glib::RefPtr<Gtk::Builder>& glade);
	virtual ~appwindow();

	bool open(const char* ui_path);

private:
	Glib::RefPtr<Gtk::Application> _app;
	Glib::RefPtr<Gtk::Builder> _builder;

	Gtk::Window* _main_window;

};

#endif /* APPWINDOW_HPP_ */
