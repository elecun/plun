/*

The MIT License (MIT)

Copyright (c) 2015 Nsynapse Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

/*
 * Usage :
 * 1) install components without dependency check
 * 		$ plun -i/--install	<any components>
 */

#include <iostream>
#include <csignal>
#include <cstdlib>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <vector>
#include <string>
#include <memory>

#include "../include/plunframework.hpp"

using namespace boost;
using namespace std;

std::shared_ptr<boost::asio::io_service::work>	service_work;
std::shared_ptr<spdlog::logger> _console = spdlog::stdout_logger_mt("main_thread");


void destroy()
{
	manager::component_manager::get()->destroy();
	broker::component_broker::get()->destroy();
	cout << "bye." << endl;
}

void close()
{
	service_work.reset();
	destroy();
	exit(0);
}

void signal_handler(int h)
{
	close();
}

int main(int argc, char* argv[])
{

	boost::asio::io_service io_service;
	service_work.reset(new boost::asio::io_service::work(io_service));

	signal(SIGINT, signal_handler);

	namespace option = boost::program_options;
	option::options_description op("Options");

	vector<string> component_list;
	op.add_options()
			("version", "program version")
			("help", "help")
			("run", option::value<std::vector<std::string>>(&component_list)->multitoken(), "run with components");

	option::options_description od("Options for developer");
	od.add_options()
			("createservice", option::value<string>(), "create service class for service component with profile")
			("createassist", option::value<string>(), "create assist class for assist component with profile")
			("createapp", option::value<string>(), "create application class for app component with profile");

	option::options_description co;
	co.add(op).add(od);

	option::variables_map vm;
	try {
		option::store(option::command_line_parser(argc, argv).options(co).style(option::command_line_style::unix_style ^ option::command_line_style::allow_short).run(), vm);


		if(vm.count("help")) { std::cout << co << std::endl; }
		else if(vm.count("version")) { std::cout << PLUN_VERSION << " (" << __DATE__ << " " <<__TIME__ << ")" << std::endl; }
		else if(vm.count("run")) {
			std::vector<std::string> components = vm["run"].as<std::vector<std::string>>();
			for(auto& component : components){
				manager::component_manager::get()->install(component.c_str(), argc, argv);
			}
			manager::component_manager::get()->run_all();
		}
		else { std::cout << co << std::endl; }
		option::notify(vm);
	}
	catch(option::error& e)
	{
	  std::cerr << "Error: " << e.what() << std::endl << std::endl;
	}

	if(manager::component_manager::get()->count()>0)
	{
		try
		{
			spdlog::get("main_thread")->info("PLUN Engine is running...");
			io_service.run();
		}
		/*catch(std::thread_interrupted& e)
		{
			service_work.reset();
			destroy();
		}*/
		catch(std::exception& e)
		{
			spdlog::get("main_thread")->error("PLUN Framework throws exception");
		}
	}

	destroy();
	exit(0);
	return 0;
}
